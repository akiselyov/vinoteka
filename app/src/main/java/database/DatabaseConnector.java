package database;

/**
 * Created by Android on 25.03.2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseConnector {
    private static final String LOG_TAG = "myLogs";
    private static final String DATABASE = "DATABASE";
    // database name

    private static final String DATABASE_NAME = "Vinoteka";
    //Tables
    private static final String WINES_TABLE = "wines";
    private static final String LINKS_TABLE = "links";
    private static final String SOMMELIER_RATING_TABLE = "sommelierratinginformation";
    private static final String PRODUCERS_TABLE = "producers";
    private static final String TYPES_TABLE = "types";
    private static final String VINTAGE_TABLE = "vintages";
    private static final String PEOPLES_TABLE = "peoples";
    private static final String COORDINATES_TABLE = "coordinates";
    private static final String SOMMELIER_100_TABLE = "sommelier_100_table";

    private SQLiteDatabase database; // database object
    private final DatabaseOpenHelper databaseOpenHelper; // database helper

    //Queries
    private static final String SQL_SELECT_ONE_POSITION = "select " + WINES_TABLE + "._id as _id, " + WINES_TABLE + ".winename as winename, "
            + WINES_TABLE + ".image as image, "
            + PRODUCERS_TABLE + ".producer as producer, "
            + COORDINATES_TABLE + ".latitude as latitude, "
            + COORDINATES_TABLE + ".longitude as longitude, "
            + TYPES_TABLE + ".type as type, "
            + VINTAGE_TABLE + ".vintage as vintage, "
            + LINKS_TABLE + ".internet_link as internet_link, "
            + SOMMELIER_RATING_TABLE + ".datetasting as datetasting, "
            + SOMMELIER_RATING_TABLE + ".notes as notes, "
            + SOMMELIER_RATING_TABLE + ".mainmark as mainmark, "
            + SOMMELIER_RATING_TABLE + ".colormark as colormark, "
            + SOMMELIER_RATING_TABLE + ".aromamark as aromamark, "
            + SOMMELIER_RATING_TABLE + ".tastemark as tastemark, "
            + SOMMELIER_RATING_TABLE + ".terroir as terroir, "
            + SOMMELIER_RATING_TABLE + ".spontaneousmark as spontaneousmark, "
            + SOMMELIER_100_TABLE + ".mainmark100 as mainmark100, "
            + SOMMELIER_100_TABLE + ".colormark100 as colormark100, "
            + SOMMELIER_100_TABLE + ".tastemark100 as tastemark100, "
            + SOMMELIER_100_TABLE + ".typicality100 as typicality100, "
            + SOMMELIER_100_TABLE + ".aromamark100 as aromamark100, "
            + PEOPLES_TABLE + ".person as person "
            + "from " + WINES_TABLE + " as " + WINES_TABLE + " "
            + "left outer join " + PRODUCERS_TABLE + " as " + PRODUCERS_TABLE + " "
            + "on " + PRODUCERS_TABLE + "._id = " + WINES_TABLE + ".producer_id  "
            + "left outer join " + COORDINATES_TABLE + " as " + COORDINATES_TABLE + " "
            + "on " + COORDINATES_TABLE + ".wine_id = " + WINES_TABLE + "._id "
            + "left outer join " + TYPES_TABLE + " as " + TYPES_TABLE + " "
            + "on " + TYPES_TABLE + "._id = " + WINES_TABLE + ".type_id "
            + "left outer join " + VINTAGE_TABLE + " as " + VINTAGE_TABLE + " "
            + "on " + VINTAGE_TABLE + "._id = " + WINES_TABLE + ".vintage_id "
            + "left outer join " + LINKS_TABLE + " as " + LINKS_TABLE + " "
            + "on " + LINKS_TABLE + ".vine_id = " + WINES_TABLE + "._id "
            + "left outer join " + SOMMELIER_RATING_TABLE + " as " + SOMMELIER_RATING_TABLE + " "
            + "on " + SOMMELIER_RATING_TABLE + ".vine_id = " + WINES_TABLE + "._id "
            + "left outer join " + SOMMELIER_100_TABLE + " as " + SOMMELIER_100_TABLE + " "
            + "on " + SOMMELIER_100_TABLE + ".vine_id = " + WINES_TABLE + "._id "
            + "left outer join " + PEOPLES_TABLE + " as " + PEOPLES_TABLE + " "
            + "on " + PEOPLES_TABLE + "._id = " + SOMMELIER_RATING_TABLE + ".person_id "
            + "where " + WINES_TABLE + "._id = ?";

    private static final String SQL_SELECT_ALL_POSITIONS = "select " + WINES_TABLE + "._id as _id, " + WINES_TABLE + ".winename as winename, "
            + WINES_TABLE + ".image as image, "
            + PRODUCERS_TABLE + ".producer as producer, "
            + COORDINATES_TABLE + ".latitude as latitude, "
            + COORDINATES_TABLE + ".longitude as longitude, "
            + TYPES_TABLE + ".type as type, "
            + VINTAGE_TABLE + ".vintage as vintage, "
            + LINKS_TABLE + ".internet_link as internet_link, "
            + SOMMELIER_RATING_TABLE + ".datetasting as datetasting, "
            + SOMMELIER_RATING_TABLE + ".notes as notes, "
            + SOMMELIER_RATING_TABLE + ".mainmark as mainmark, "
            + SOMMELIER_RATING_TABLE + ".colormark as colormark, "
            + SOMMELIER_RATING_TABLE + ".aromamark as aromamark, "
            + SOMMELIER_RATING_TABLE + ".tastemark as tastemark, "
            + SOMMELIER_RATING_TABLE + ".terroir as terroir, "
            + SOMMELIER_RATING_TABLE + ".spontaneousmark as spontaneousmark, "
            + PEOPLES_TABLE + ".person as person "
            + "from " + WINES_TABLE + " as " + WINES_TABLE + " "
            + "left outer join " + PRODUCERS_TABLE + " as " + PRODUCERS_TABLE + " "
            + "on " + PRODUCERS_TABLE + "._id = " + WINES_TABLE + ".producer_id  "
            + "left outer join " + COORDINATES_TABLE + " as " + COORDINATES_TABLE + " "
            + "on " + COORDINATES_TABLE + ".wine_id = " + WINES_TABLE + "._id "
            + "left outer join " + TYPES_TABLE + " as " + TYPES_TABLE + " "
            + "on " + TYPES_TABLE + "._id = " + WINES_TABLE + ".type_id "
            + "left outer join " + VINTAGE_TABLE + " as " + VINTAGE_TABLE + " "
            + "on " + VINTAGE_TABLE + "._id = " + WINES_TABLE + ".vintage_id "
            + "left outer join " + LINKS_TABLE + " as " + LINKS_TABLE + " "
            + "on " + LINKS_TABLE + ".vine_id = " + WINES_TABLE + "._id "
            + "left outer join " + SOMMELIER_RATING_TABLE + " as " + SOMMELIER_RATING_TABLE + " "
            + "on " + SOMMELIER_RATING_TABLE + ".vine_id = " + WINES_TABLE + "._id "
            + "left outer join " + PEOPLES_TABLE + " as " + PEOPLES_TABLE + " "
            + "on " + PEOPLES_TABLE + "._id = " + SOMMELIER_RATING_TABLE + ".person_id order by ";

    // public constructor for DatabaseConnector
    public DatabaseConnector(Context context) {
        // create a new DatabaseOpenHelper
        databaseOpenHelper = new DatabaseOpenHelper(context, DATABASE_NAME, null, 1);
    } // end DatabaseConnector constructor

    // open the database connection
    public void open() throws SQLException {
        // create or open a database for reading/writing
        database = databaseOpenHelper.getWritableDatabase();
        Log.d(LOG_TAG, DATABASE + ": Open()");

        Log.d(LOG_TAG, "--- Rows in " + WINES_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = database.query(WINES_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int vinenameColIndex = c.getColumnIndex("winename");
            int imageColIndex = c.getColumnIndex("image");
            int producer_idColIndex = c.getColumnIndex("producer_id");
            int type_idColIndex = c.getColumnIndex("type_id");
            int vintage_idColIndex = c.getColumnIndex("vintage_id");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", vinename = " + c.getString(vinenameColIndex) +
                        ", image = " + c.getString(imageColIndex) +
                        ", producer_id = " + c.getInt(producer_idColIndex) +
                        ", type_id = " + c.getInt(type_idColIndex) +
                        ", vintage_id = " + c.getInt(vintage_idColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + PRODUCERS_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(PRODUCERS_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int producer_ColIndex = c.getColumnIndex("producer");
            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", producer = " + c.getString(producer_ColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + COORDINATES_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(COORDINATES_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int latitude_ColIndex = c.getColumnIndex("latitude");
            int longitude_ColIndex = c.getColumnIndex("longitude");
            int wine_idColIndex = c.getColumnIndex("wine_id");
            int producer_idColIndex = c.getColumnIndex("producer_id");
            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", latitude = " + c.getString(latitude_ColIndex) +
                        ", longitude = " + c.getString(longitude_ColIndex) +
                        ",wine_id = " + c.getString(wine_idColIndex) +
                        ",producer_id = " + c.getString(producer_idColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + TYPES_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(TYPES_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int types_ColIndex = c.getColumnIndex("type");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", type = " + c.getString(types_ColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + VINTAGE_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(VINTAGE_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int vintage_ColIndex = c.getColumnIndex("vintage");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", vintage = " + c.getString(vintage_ColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + LINKS_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(LINKS_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int vine_id_ColIndex = c.getColumnIndex("vine_id");
            int link_ColIndex = c.getColumnIndex("internet_link");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", vine_id = " + c.getInt(vine_id_ColIndex) +
                        ", link = " + c.getString(link_ColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + SOMMELIER_RATING_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(SOMMELIER_RATING_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int vine_id_ColIndex = c.getColumnIndex("vine_id");
            int people_id_ColIndex = c.getColumnIndex("person_id");
            int datetasting_ColIndex = c.getColumnIndex("datetasting");
            int notes_ColIndex = c.getColumnIndex("notes");
            int mainmark_ColIndex = c.getColumnIndex("mainmark");
            int colormark_ColIndex = c.getColumnIndex("colormark");
            int aromamark_ColIndex = c.getColumnIndex("aromamark");
            int tastemark_ColIndex = c.getColumnIndex("tastemark");
            int terroir_ColIndex = c.getColumnIndex("terroir");
            int spontaneousmark_ColIndex = c.getColumnIndex("spontaneousmark");

            do {
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", vine_id = " + c.getInt(vine_id_ColIndex) +
                        ", people_id = " + c.getInt(people_id_ColIndex) +
                        ", datetasting = " + c.getString(datetasting_ColIndex) +
                        ", notes = " + c.getString(notes_ColIndex) +
                        ", mainmark = " + c.getDouble(mainmark_ColIndex) +
                        ", colormark = " + c.getInt(colormark_ColIndex) +
                        ", aromamark = " + c.getInt(aromamark_ColIndex) +
                        ", tastemark = " + c.getInt(tastemark_ColIndex) +
                        ", terroir = " + c.getInt(terroir_ColIndex) +
                        ", spontaneousmark = " + c.getString(spontaneousmark_ColIndex));
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + PEOPLES_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(PEOPLES_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int person_ColIndex = c.getColumnIndex("person");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", person = " + c.getInt(person_ColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

        Log.d(LOG_TAG, "--- Rows in " + SOMMELIER_100_TABLE + ": ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        c = database.query(SOMMELIER_100_TABLE, null, null, null, null, null, null);
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");
            int vine_id_ColIndex = c.getColumnIndex("vine_id");
            int mainmark_ColIndex = c.getColumnIndex("mainmark100");
            int colormark_ColIndex = c.getColumnIndex("colormark100");
            int aromamark_ColIndex = c.getColumnIndex("aromamark100");
            int tastemark_ColIndex = c.getColumnIndex("tastemark100");
            int typicality_ColIndex = c.getColumnIndex("typicality100");
            do {
                Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) +
                        ", vine_id = " + c.getInt(vine_id_ColIndex) +
                        ", mainmark = " + c.getInt(mainmark_ColIndex) +
                        ", colormark = " + c.getInt(colormark_ColIndex) +
                        ", aromamark = " + c.getInt(aromamark_ColIndex) +
                        ", tastemark = " + c.getInt(tastemark_ColIndex) +
                        ", typicality = " + c.getInt(typicality_ColIndex));
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();

    } // end method open

    // close the database connection
    public void close() {
        if (database != null) {
            database.close(); // close the database connection
            Log.d(LOG_TAG, DATABASE + ": Close()");
        }
    } // end method close

    public void insertNewPosition(String winename, String image, String vintage, String producer,
                                  String type, String internet_link, String person, String datetasting, String notes,
                                  String mainmark, String colormark, String aromamark, String tastemark, String terroir, String spontaneousmark, double longitude, double latitude,
                                  String mainmark100, String colormark100, String aromamark100, String tastemark100, String typicality100) {
        open(); // open the database
        Log.d(LOG_TAG, DATABASE + ": Insert()");

        ContentValues newPosition = new ContentValues();
        newPosition.put("producer", producer);
        if (!producer.equals(""))
            database.execSQL("INSERT INTO " + PRODUCERS_TABLE + " (producer)" + " SELECT '" + producer + "'" + " temp WHERE NOT EXISTS (SELECT 1 FROM " + PRODUCERS_TABLE +
                    " WHERE producer = temp)");

        newPosition.clear();

        newPosition.put("vintage", vintage);
        if (!vintage.equals(""))
            database.execSQL("INSERT INTO " + VINTAGE_TABLE + " (vintage)" + " SELECT '" + vintage + "' temp WHERE NOT EXISTS (SELECT 1 FROM " + VINTAGE_TABLE + " WHERE vintage = temp)");
        newPosition.clear();

        newPosition.put("type", type);
        if (!type.equals(""))
            database.execSQL("INSERT INTO " + TYPES_TABLE + " (type)" + " SELECT '" + type + "' temp WHERE NOT EXISTS (SELECT 1 FROM " + TYPES_TABLE + " WHERE type = temp)");
        newPosition.clear();

        Cursor c = database.query(PRODUCERS_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            newPosition.put("producer_id", c.getInt(c.getColumnIndex("_id")));

        c = database.query(VINTAGE_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            newPosition.put("vintage_id", c.getInt(c.getColumnIndex("_id")));

        c = database.query(TYPES_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            newPosition.put("type_id", c.getInt(c.getColumnIndex("_id")));

        newPosition.put("winename", winename);
        if (image != null)
            newPosition.put("image", image);
        database.insert(WINES_TABLE, null, newPosition);
        newPosition.clear();

        c = database.query(WINES_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1) {
            newPosition.put("vine_id", c.getInt(c.getColumnIndex("_id")));
            newPosition.put("internet_link", internet_link);
        }

        if (!internet_link.equals(""))
            database.insert(LINKS_TABLE, null, newPosition);
        newPosition.clear();

        c = database.query(WINES_TABLE, new String[]{"_id"},
                null, null, null, null,"_id");
        Cursor b = database.query(PRODUCERS_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        b.moveToLast();
        if(c.getPosition() != -1) {
            newPosition.put("wine_id",c.getInt(c.getColumnIndex("_id")));
            newPosition.put("producer_id",c.getInt(c.getColumnIndex("_id")));
            newPosition.put("latitude",latitude);
            newPosition.put("longitude",longitude);
        }
        if(latitude != -1 && longitude != -1)
            database.insert(COORDINATES_TABLE, null, newPosition);
        newPosition.clear();

        newPosition.put("person", person);
        if (!person.equals(""))
            database.execSQL("INSERT INTO " + PEOPLES_TABLE + " (person)" + " SELECT '" + person + "' temp WHERE NOT EXISTS (SELECT 1 FROM " + PEOPLES_TABLE + " WHERE person = temp)");
        newPosition.clear();

        c = database.query(PEOPLES_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            newPosition.put("person_id", c.getInt(c.getColumnIndex("_id")));
        c = database.query(WINES_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            newPosition.put("vine_id", c.getInt(c.getColumnIndex("_id")));
        if (notes != null)
            newPosition.put("notes", notes);
        if (!datetasting.equals(""))
            newPosition.put("datetasting", datetasting);
        if (!mainmark.equals(""))
            newPosition.put("mainmark", mainmark);
        if (!colormark.equals(""))
            newPosition.put("colormark", colormark);
        if (!aromamark.equals(""))
            newPosition.put("aromamark", aromamark);
        if (!tastemark.equals(""))
            newPosition.put("tastemark", tastemark);
        if (!terroir.equals(""))
            newPosition.put("terroir", terroir);
        newPosition.put("spontaneousmark", spontaneousmark);

        if (!datetasting.equals(""))
            database.insert(SOMMELIER_RATING_TABLE, null, newPosition);
        newPosition.clear();

        c = database.query(WINES_TABLE, new String[]{"_id"},
                null,null,null,null,"_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            newPosition.put("vine_id", c.getInt(c.getColumnIndex("_id")));
        if (!mainmark100.equals(""))
            newPosition.put("mainmark100",mainmark100);
        if (!colormark100.equals(""))
            newPosition.put("colormark100",colormark100);
        if (!aromamark100.equals(""))
            newPosition.put("aromamark100",aromamark100);
        if (!typicality100.equals(""))
            newPosition.put("typicality100",typicality100);
        if (!tastemark100.equals(""))
            newPosition.put("tastemark100",tastemark100);

        if (!datetasting.equals(""))
            database.insert(SOMMELIER_100_TABLE,null,newPosition);
        newPosition.clear();
        close(); // close the database

    }


        // inserts a new contact in the database
    public void updatePosition(long id, String winename, String image, String internet_link, String datetasting, String notes,
                               String mainmark, String colormark, String aromamark, String tastemark, String terroir, String spontaneousmark, String producer, String type, String vintage,
                               String mainmark100, String colormark100, String tastemark100, String typicality100, String aromamark100) {
        Log.d(LOG_TAG, DATABASE + ": Update() ID = " + Long.toString(id));

        open(); // open the database

        ContentValues newPosition = new ContentValues();
        newPosition.put("producer", producer);
        if (!producer.equals(""))
            database.execSQL("INSERT INTO " + PRODUCERS_TABLE + " (producer)" + " SELECT '" + producer + "'" + " temp WHERE NOT EXISTS (SELECT 1 FROM " + PRODUCERS_TABLE +
                    " WHERE producer = temp)");

        newPosition.clear();

        newPosition.put("vintage", vintage);
        if (!vintage.equals(""))
            database.execSQL("INSERT INTO " + VINTAGE_TABLE + " (vintage)" + " SELECT '" + vintage + "' temp WHERE NOT EXISTS (SELECT 1 FROM " + VINTAGE_TABLE + " WHERE vintage = temp)");
        newPosition.clear();

        newPosition.put("type", type);
        if (!type.equals(""))
            database.execSQL("INSERT INTO " + TYPES_TABLE + " (type)" + " SELECT '" + type + "' temp WHERE NOT EXISTS (SELECT 1 FROM " + TYPES_TABLE + " WHERE type = temp)");
        newPosition.clear();

        int producer_id = 0;
        int vintage_id = 0;
        int type_id = 0;

        Cursor c = database.query(PRODUCERS_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            producer_id = c.getInt(c.getColumnIndex("_id"));

        c = database.query(VINTAGE_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            vintage_id = c.getInt(c.getColumnIndex("_id"));

        c = database.query(TYPES_TABLE, new String[]{"_id"},
                null, null, null, null, "_id");
        c.moveToLast();
        if (c.getPosition() != -1)
            type_id = c.getInt(c.getColumnIndex("_id"));

        String SQL_UPDATE_WINES_TABLE = "UPDATE " + WINES_TABLE + " SET winename = '" + winename + "', vintage_id = '" + vintage_id + "', producer_id = '" + producer_id + "', type_id = '" + type_id + "', image = '" + image + "' where _id = " + Long.toString(id);
        database.execSQL(SQL_UPDATE_WINES_TABLE);
        Log.d(LOG_TAG, "UPDATE WINE_TABLE: " + SQL_UPDATE_WINES_TABLE + " " + Long.toString(id));

        String SQL_UPDATE_LINKS_TABLE = "UPDATE " + LINKS_TABLE + " SET internet_link = '" + internet_link + "' where vine_id = " + Long.toString(id);
        database.execSQL(SQL_UPDATE_LINKS_TABLE);
        Log.d(LOG_TAG, "UPDATE LINKS_TABLE: " + SQL_UPDATE_LINKS_TABLE);

        String SQL_UPDATE_SOMMELIER_RATING_TABLE = "UPDATE " + SOMMELIER_RATING_TABLE + " SET datetasting = '" + datetasting + "', notes = '" +
                notes + "', mainmark = '" + mainmark + "', colormark = '" + colormark + "', aromamark = '" + aromamark +
                "', tastemark = '" + tastemark + "', terroir = '" + terroir + "', spontaneousmark = '" + spontaneousmark + "' where vine_id = " + Long.toString(id);
        database.execSQL(SQL_UPDATE_SOMMELIER_RATING_TABLE);
        Log.d(LOG_TAG, "UPDATE SOMMELIER_RATING_TABLE: " + SQL_UPDATE_SOMMELIER_RATING_TABLE);

        String SQL_UPDATE_SOMMELIER_100_TABLE = "UPDATE " + SOMMELIER_100_TABLE + " SET mainmark100 = '" + mainmark100 + "', colormark100 = '" + colormark100 + "', aromamark100 = '" + aromamark100 +
                "', tastemark100 = '" + tastemark100 + "', typicality100 = '" + typicality100 + "' where vine_id = " + Long.toString(id);
        database.execSQL(SQL_UPDATE_SOMMELIER_100_TABLE);
        Log.d(LOG_TAG, "UPDATE SOMMELIER_100_TABLE: " + SQL_UPDATE_SOMMELIER_100_TABLE);
        close(); // close the database
    }


    public Cursor getAllPositions(String parameter) {
        return database.rawQuery(SQL_SELECT_ALL_POSITIONS + parameter, null);
    }

    public Cursor getList(String parameter)
    {
        if(parameter.equals("vintage"))
            return database.query(VINTAGE_TABLE, new String[] {"_id","vintage"},
                            null, null, null, null, "vintage");
        else if(parameter.equals("producer"))
            return database.query(PRODUCERS_TABLE, new String[] {"_id","producer"},
                    null, null, null, null, "producer");
        else if(parameter.equals("type"))
            return database.query(TYPES_TABLE, new String[] {"_id","type"},
                    null, null, null, null, "type");
        else
            return database.query(PEOPLES_TABLE, new String[] {"_id","person"},
                    null, null, null, null, "person");
    }

    private static final String SQL_SELECT_DIAGRAM_PRODUCERS = "select producers.producer as producer, producers._id as producer_id, count(wines._id) as count " +
            "from wines join producers where wines.producer_id = producers._id group by producers._id";
    private static final String SQL_SELECT_DIAGRAM_VINTAGES = "select vintages.vintage as vintage, vintages._id as vintage_id, count(wines._id) as count " +
            "from wines join vintages where wines.vintage_id = vintages._id group by vintages._id";
    private static final String SQL_SELECT_DIAGRAM_TYPES = "select types.type as type, types._id as type_id, count(wines._id) as count " +
            "from wines join types where wines.type_id = types._id group by types._id";

    private static final String SQL_SELECT_COORDINATES = "select wines.winename as winename, coordinates.latitude as latitude, coordinates.longitude as longitude from "+WINES_TABLE + " as " + WINES_TABLE + " "
            + "left outer join " + COORDINATES_TABLE + " as " + COORDINATES_TABLE + " "
            + "on " + COORDINATES_TABLE + ".wine_id = " + WINES_TABLE + "._id  order by " +WINES_TABLE+".winename";

    public Cursor getCoordinates() {
        return database.rawQuery(SQL_SELECT_COORDINATES,null);
    }

    public Cursor getDiagramData(String type) {
        Cursor result = null;
        switch(type) {
            case("producer"):
                result = database.rawQuery(SQL_SELECT_DIAGRAM_PRODUCERS,null);
                break;
            case("vintage"):
                result = database.rawQuery(SQL_SELECT_DIAGRAM_VINTAGES,null);
                break;
            case("type"):
                result = database.rawQuery(SQL_SELECT_DIAGRAM_TYPES,null);
                break;
            default:
                break;
        }
        return result;
    }

    private static final String SQL_SELECT_GRAPHIC = "select sommelierratinginformation.datetasting as date, sommelierratinginformation.mainmark as mark, wines.winename as wine from sommelierratinginformation join wines where wines._id = sommelierratinginformation.vine_id order by date";
    public Cursor getGraphicData() {
        Cursor result = database.rawQuery(SQL_SELECT_GRAPHIC,null);
        return result;
    }

    // get a Cursor containing all information about the contact specified
    // by the given id
    public Cursor getOnePosition(long id)
    {
        Cursor result = database.rawQuery(SQL_SELECT_ONE_POSITION, new String[] {Long.toString(id)});
        Log.d(LOG_TAG,SQL_SELECT_ONE_POSITION);
        return result;
    } // end method getOnContact

    //Поиск запросом LIKE
    public Cursor fetchWinesByQuery(String query) {
        return database.query(true, WINES_TABLE, new String[] { "_id",
                        "winename" }, "winename" + " LIKE" + "'%" + query + "%'", null,
                null, null, null, null);
    }
    // delete the contact specified by the given String name
    public void deletePosition(long id)
    {
        Log.d(LOG_TAG, DATABASE + ": Delete() ID = " + Long.toString(id));
        open(); // open the database
        database.delete(SOMMELIER_RATING_TABLE, "vine_id=" + id, null);
        database.delete(SOMMELIER_100_TABLE,"vine_id="+id, null);
        database.delete(LINKS_TABLE, "vine_id=" + id, null);
        database.delete(WINES_TABLE, "_id=" + id, null);
        database.delete(COORDINATES_TABLE,"wine_id=" + id, null);
        close(); // close the database
    } // end method deleteContact

    private class DatabaseOpenHelper extends SQLiteOpenHelper
    {
        private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
                + SOMMELIER_RATING_TABLE + ", " + LINKS_TABLE + ", " + WINES_TABLE + ", " + PRODUCERS_TABLE + ", " + TYPES_TABLE + ", " + VINTAGE_TABLE + ", " + PEOPLES_TABLE;
        private static final String SQL_CREATE_SOMMELIER_RATING_TABLE = "create table " + SOMMELIER_RATING_TABLE + " (" + "_id integer primary key autoincrement,"
                + "vine_id integer, person_id integer, datetasting text, notes text, mainmark text, colormark text, aromamark text, tastemark text, terroir text, spontaneousmark text, "
                + "FOREIGN KEY(vine_id) REFERENCES wines(_id), FOREIGN KEY(person_id) REFERENCES peoples(_id));";
        private static final String SQL_CREATE_SOMMELIER_100RATING_TABLE = "create table " + SOMMELIER_100_TABLE + " (" + "_id integer primary key autoincrement,"
                + "vine_id integer, mainmark100 text, colormark100 text, aromamark100 text, tastemark100 text, typicality100 text, "
                + "FOREIGN KEY(vine_id) REFERENCES wines(_id));";
        private static final String SQL_CREATE_LINKS_TABLE = "create table " + LINKS_TABLE + " (" + "_id integer primary key autoincrement,"
                + "vine_id integer, internet_link text, " + "FOREIGN KEY(vine_id) REFERENCES wines(_id));";
        private static final String SQL_CREATE_WINES_TABLE = "create table " + WINES_TABLE + " (" + "_id integer primary key autoincrement," + "winename text,"
                + "image text, vintage_id integer, producer_id integer, type_id integer, " + "FOREIGN KEY(producer_id) REFERENCES producers(_id),"
                + "FOREIGN KEY(vintage_id) REFERENCES vintages(_id), FOREIGN KEY(type_id) REFERENCES types(_id));";
        private static final String SQL_CREATE_PRODUCERS_TABLE = "create table " + PRODUCERS_TABLE + " (" + "_id integer primary key autoincrement," + "producer text" +");";
        private static final String SQL_CREATE_TYPES_TABLE = "create table " + TYPES_TABLE + " (" + "_id integer primary key autoincrement," + "type text" + ");";
        private static final String SQL_CREATE_VINTAGE_TABLE = "create table " + VINTAGE_TABLE + " (" + "_id integer primary key autoincrement," + "vintage text" + ");";
        private static final String SQL_CREATE_PEOPLES_TABLE = "create table " + PEOPLES_TABLE + " (" + "_id integer primary key autoincrement," + "person text" + ");";
        private static final String SQL_CREATE_COORDINATES_TABLE = "create table " + COORDINATES_TABLE + " (" + "_id integer primary key autoincrement," + "latitude real, longitude real, wine_id integer, producer_id integer, "
                + "FOREIGN KEY(wine_id) REFERENCES wines(_id),FOREIGN KEY(producer_id) REFERENCES producers(_id));";
        // public constructor
        public DatabaseOpenHelper(Context context, String name,
                                  CursorFactory factory, int version)
        {
            super(context, name, factory, version);
        } // end DatabaseOpenHelper constructor

        // creates the contacts table when the database is created
        @Override
        public void onCreate(SQLiteDatabase db)
        {
            Log.w(LOG_TAG, "Создание базы данных");
         //   db.execSQL(SQL_DELETE_ENTRIES);
            // Create  tables
            db.execSQL(SQL_CREATE_PRODUCERS_TABLE);

            db.execSQL(SQL_CREATE_TYPES_TABLE);

            db.execSQL(SQL_CREATE_VINTAGE_TABLE);

            db.execSQL(SQL_CREATE_PEOPLES_TABLE);

            db.execSQL(SQL_CREATE_WINES_TABLE);

            db.execSQL(SQL_CREATE_SOMMELIER_RATING_TABLE);

            db.execSQL(SQL_CREATE_SOMMELIER_100RATING_TABLE);

            db.execSQL(SQL_CREATE_LINKS_TABLE);

            db.execSQL(SQL_CREATE_COORDINATES_TABLE);
          //  db.execSQL(TRIGGER);
        } // end method onCreate

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion)
        {
            Log.w(LOG_TAG, "Обновление базы данных с версии " + oldVersion
                + " до версии " + newVersion + ", которое удалит все старые данные");
            // Удаляем предыдущую таблицу при апгрейде
            //db.execSQL(SQL_DELETE_ENTRIES);
            // Создаём новый экземпляр таблицы
            //onCreate(db);
        } // end method onUpgrade
    } // end class DatabaseOpenHelper

} // end class DatabaseConnector