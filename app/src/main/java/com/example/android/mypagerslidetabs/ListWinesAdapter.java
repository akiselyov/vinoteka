package com.example.android.mypagerslidetabs;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

class ListWinesAdapter extends SimpleCursorAdapter
{
    private final static String LOG_TAG = "myLogs";
    private final Map<Integer, Long> mapPositionID;
	private final ListWinesFragment fragment;
    private Cursor curs;


    public ListWinesAdapter(Context context, int item, Cursor cursor, ListWinesFragment fragment, String [] from,int [] to) {
        super(context,item,cursor,from,to);
        this.curs = cursor;
        this.mapPositionID = new HashMap<Integer, Long>();
		this.fragment = fragment;
	}

    @Override
    public void bindView(@NonNull View view, Context context, @NonNull Cursor cursor) {

    }

    @Override
    public View newView (Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.position_list_item, parent, false);
        bindView(view, context, cursor);
        return view;
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
        if(curs != null)
            return curs.getCount();
        else
            return 0;
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return curs.getString(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

    public long getIdByPosition(int position) {
        return mapPositionID.get(position);
    }

	public static class ViewHolder
	{
		public TextView txtViewVineName;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder view;
		LayoutInflater inflator = fragment.getActivity().getLayoutInflater();

        if (convertView == null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.position_list_item, null);
            view.txtViewVineName = (TextView) convertView.findViewById(R.id.positionview);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        this.curs.moveToPosition(position);
        view.txtViewVineName.setText(this.curs.getString(this.curs.getColumnIndex(Constants.WINENAME)));
        mapPositionID.put(position, Long.parseLong(this.curs.getString(this.curs.getColumnIndex(Constants.ID))));
		return convertView;
	}

    @Override
    public void changeCursor(Cursor cursor) {
        this.curs = cursor;
    }

}
