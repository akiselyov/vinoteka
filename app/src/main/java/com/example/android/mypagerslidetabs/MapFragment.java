package com.example.android.mypagerslidetabs;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import database.DatabaseConnector;

/**
 * Created by Android on 11.06.2015.
 */
public class MapFragment extends Fragment {

    private final static String LOG_TAG = "LOg";


    private ArrayList<Coordinates> coordinates;
    private String [] winearray = {};
    private int winenames_length;

    private GoogleMap googleMap;

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
       // fragment.setArguments(args);
        return fragment;
    }

    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == googleMap) {
                googleMap = ((com.google.android.gms.maps.MapFragment) getActivity().getFragmentManager().findFragmentById(
                        R.id.mainmapfrag)).getMap();
                /**
                 * If the map is still null after attempted initialisation,
                 * show an error to the user
                 */
                if (null == googleMap) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("mapApp", exception.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean markerExist = false;
    }

    private static View rootView;
    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
            try {
                rootView = inflater.inflate(R.layout.mainmap_fragment, container, false);

            } catch (InflateException e) {
        /* map is already there, just return view as it is  */
        }

        createMapView();
        GetCoordinatesTask getCoordinatesTask = new GetCoordinatesTask();
        getCoordinatesTask.execute();

        return rootView;
    }


    // performs database query outside GUI thread
    private class GetCoordinatesTask extends AsyncTask<Object, Object, Cursor>
    {
        final DatabaseConnector databaseConnector =
                new DatabaseConnector(MapFragment.this.getActivity());
        // обнуляем ссылку
        // perform the database access
        @Override
        protected Cursor doInBackground(Object... params)
        {
            Log.d(LOG_TAG, "GetContactTask: doInBackGround()");

            databaseConnector.open();
            return databaseConnector.getCoordinates();
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            Log.d(LOG_TAG, "GetCoordinatesTask: onPostExecute()");
            int winenameIndex = result.getColumnIndex(Constants.WINENAME);
            int latitudeIndex = result.getColumnIndex(Constants.LATITUDE);
            int longitudeIndex = result.getColumnIndex(Constants.LONGITUDE);

            Log.d("marker","count = " + result.getCount());

            coordinates = new ArrayList<>();
            winearray = new String[result.getCount()];
            winenames_length = result.getCount();
            if(result.getCount() != 0) {
                result.moveToFirst();
                int i = 0;
                do {
                    coordinates.add(new Coordinates(result.getDouble(latitudeIndex), result.getDouble(longitudeIndex)));
                    winearray[i] = result.getString(winenameIndex);
                    i++;
                } while (result.moveToNext());
                databaseConnector.close();
                for (i = 0; i < winearray.length; i++) {
                    LatLng dragPosition = new LatLng(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude());
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(dragPosition);
                    markerOptions.title(winearray[i]);
                    markerOptions.draggable(true);
                    //create new marker when user long clicks
                    googleMap.addMarker(markerOptions);
                }
            }
            //     databaseConnector.close();
        } // end method onPostExecute
    }
}
