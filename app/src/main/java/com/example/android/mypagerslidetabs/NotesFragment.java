package com.example.android.mypagerslidetabs;

/**
 * Created by Android on 06.04.2015.
 */
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class NotesFragment extends DialogFragment implements OnClickListener {

    private final String LOG_TAG = "myLogs";
    private EditText share_content;
    private String filename;

    public void setFileName(String fileName) {
        this.filename = fileName;
    }

    public String getFileName() {
        return this.filename;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Заметка о вине");
        View v = inflater.inflate(R.layout.dialog_fragment, null);
        v.findViewById(R.id.saveNoteButton).setOnClickListener(this);
        v.findViewById(R.id.clearNoteButton).setOnClickListener(this);
        share_content = (EditText) v.findViewById(R.id.share_content);
        FileManager fm = new FileManager(NotesFragment.this.getActivity());
        share_content.setText(fm.readFile(this.filename));
        return v;
    }

    public void onClick(View v) {
        Log.d(LOG_TAG, "NotesFragment: " + ((Button) v).getText());

        FileManager fileManager = new FileManager(NotesFragment.this.getActivity());
        switch (v.getId()) {
            case R.id.saveNoteButton:
                fileManager.writeFile(this.filename, share_content.getText().toString());

                break;
            case R.id.clearNoteButton:
                share_content.setText("");
                fileManager.writeFile(this.filename, share_content.getText().toString());
                break;

        }
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "NotesFragment: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "NotesFragment: onCancel");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "NotesFragment: onStart");
    }

    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "NotesFragment: onResume");
        FileManager fileManager = new FileManager(NotesFragment.this.getActivity());
        share_content.setText(fileManager.readFile(this.filename));
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "NotesFragment: onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "NotesFragment: onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "NotesFragment: onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "NotesFragment: onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "NotesFragment: onDetach");
    }

}