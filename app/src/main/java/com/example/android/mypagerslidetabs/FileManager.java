package com.example.android.mypagerslidetabs;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by Android on 01.04.2015.
 */
class FileManager {

    private File directory;
    private final int TYPE_PHOTO = 1;
    private final int TYPE_NOTE = 2;
    private final String LOG_TAG = "myLogs";
    private final static String DIR_SD = "VinotekaDirectory";
    private final Context context;

    public FileManager(Context context) {
        this.context = context;
        createDirectory();
    }

    public void writeFile(String filename, String text) {
        try {
            // отрываем поток для записи
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                    this.context.openFileOutput(filename, Context.MODE_PRIVATE)));
            // пишем данные
            bw.write(text);
            // закрываем поток
            bw.close();
            Log.d(LOG_TAG, "Файл записан");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFile(String filename) {
        try {
            // открываем поток для чтения
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    this.context.openFileInput(filename)));
            String str;
            String readText = "";
            // читаем содержимое
            while ((str = br.readLine()) != null) {
                readText = readText.concat(str);
            }
            return readText;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void writeFileSD(String filename, String text) {
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }
        Log.d(LOG_TAG,"WRITESD");
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // создаем каталог
        sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath + File.separator + filename);

        if(sdFile.exists())
        {
            Log.d(LOG_TAG,"file exist");
        }
        else
        {
            Log.d(LOG_TAG,"file does not exist");
        }
        try {
            Log.d(LOG_TAG,"WRITESD");
            // открываем поток для записи
            BufferedWriter bw = new BufferedWriter(new FileWriter(sdFile));
            // пишем данные
            bw.write(text);
            // закрываем поток
            bw.close();
            Log.d(LOG_TAG, "Файл записан на SD: " + sdFile.getAbsolutePath() + "/" + DIR_SD);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFileSD(String filename) {
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return null;
        }
        Log.d(LOG_TAG,"READSD");
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath, filename);
        Log.d(LOG_TAG, "READ: " + sdFile.getPath());
        if(sdFile.exists())
        {
            Log.d(LOG_TAG,"file exist");
        }
        else
        {
            Log.d(LOG_TAG,"file does not exist");
        }
        try {
            // открываем поток для чтения
            BufferedReader br = new BufferedReader(new FileReader(sdFile));
            String str;
            String readText = "";
            // читаем содержимое
            while ((str = br.readLine()) != null) {
                Log.d(LOG_TAG, str);
                readText = readText.concat(str);
            }
            return readText;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getPath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;
            case TYPE_NOTE:
                file = new File(directory.getPath() + "/" + "notes_"
                        + System.currentTimeMillis() + ".txt");
                break;
        }
        Log.d(LOG_TAG, "fileName = " + file);
        return Uri.fromFile(file);
    }

    public String generateFile(int type) {
        String filename = "";
        switch (type) {
            case TYPE_PHOTO:
                filename = "photo_"
                        + System.currentTimeMillis() + ".jpg";
                break;
            case TYPE_NOTE:
                filename = "notes_"
                        + System.currentTimeMillis() + ".txt";
                break;
        }
        Log.d(LOG_TAG, "fileName = " + filename);
        return filename;
    }

    void createDirectory() {
        directory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyFolder");
        if (!directory.exists())
            directory.mkdirs();
    }

}
