/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.mypagerslidetabs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import database.DatabaseConnector;

public class ListWinesFragment extends Fragment {

    private final static String LOG_TAG = "myLogs";
	private static final String ARG_POSITION = "position";
    private static final String ARG_SORTING = "sorting";
    private static final String ARG_TITLE = "title";

    private String sorting;

    private DatabaseConnector databaseConnector;

    private static final String ROW_ID = "_id"; // Intent extra key

    private ListWinesAdapter positionsAdapter; // adapter for ListView

    private ImageView imageMainView;
    private TextView wineMainTextView;
    private TextView producerMainTextView;
    private TextView vintageMainTextView;
    private TextView typeMainTextView;
    private TextView mainMarkMainTextView;
    private TextView main20MarkTextView;
    private Button btnSee;
    private Wine wine;
    private SommelierMarks sommelierMarks;
    private int wineID = -1;

	public static ListWinesFragment newInstance(int position, String Title, String sorting) {
		ListWinesFragment f = new ListWinesFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
        b.putString(ARG_SORTING, sorting);
        b.putString(ARG_TITLE, Title);

		f.setArguments(b);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        sorting = getArguments().getString(ARG_SORTING);
        databaseConnector = new DatabaseConnector(this.getActivity());
        databaseConnector.open();
        Log.d(LOG_TAG, "This parameter:" + this.sorting);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(LOG_TAG, "Fragment1 onAttach");
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

		FrameLayout fl = new FrameLayout(getActivity());
		fl.setLayoutParams(params);
        Log.d(LOG_TAG, "onCreateView fragment");
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        View view = inflater.inflate(R.layout.listfragment, null);


        // map each contact's name to a TextView in the ListView layout
        String[] from = new String[] { "winename" };
        int[] to = new int[] { R.id.positionview };
        positionsAdapter = new ListWinesAdapter(view.getContext(), R.layout.position_list_item, null, this,from, to);
        ListView positionsListView = (ListView) view.findViewById(R.id.listviewID);
        positionsListView.setAdapter(positionsAdapter); // set contactView's adapter
        positionsListView.setOnItemClickListener(viewPositionsListener);


        imageMainView = (ImageView) view.findViewById(R.id.imageMainView);
        wineMainTextView = (TextView) view.findViewById(R.id.wineMainTextView);
        vintageMainTextView = (TextView) view.findViewById(R.id.vintageMainTextView);
        producerMainTextView = (TextView) view.findViewById(R.id.producerMainTextView);
        typeMainTextView = (TextView) view.findViewById(R.id.typeMainTextView);
        mainMarkMainTextView = (TextView) view.findViewById(R.id.mainMarkMainTextView);
        main20MarkTextView = (TextView) view.findViewById(R.id.main20MarkMainTextView);
        btnSee = (Button) view.findViewById(R.id.btnSee);
        btnSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ListWinesFragment.this.wineID != -1) {
                    Intent viewPosition = new Intent(ListWinesFragment.this.getActivity(), ViewTabsWine.class);
                    // pass the selected contact's row ID as an extra with the Intent
                    viewPosition.putExtra(ROW_ID,new Long(ListWinesFragment.this.wineID));
                    startActivity(viewPosition); // start the ViewContact Activity
                }
            }
        });
        fl.addView(view);
		return fl;
	}


    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        wineMainTextView.setText("");
        imageMainView.invalidate();
        imageMainView.setImageBitmap(null);
        vintageMainTextView.setText("");
        producerMainTextView.setText("");
        typeMainTextView.setText("");
        mainMarkMainTextView.setText("");
        main20MarkTextView.setText("");
        Log.d(LOG_TAG, "This parameter:" + this.sorting);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        databaseConnector.open();
        // create new GetContactsTask and execute it
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        GetPositionsTask getPositionsTask = new GetPositionsTask();
        getPositionsTask.execute(this.sorting);
        this.positionsAdapter.notifyDataSetChanged();
        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        Cursor cursor = positionsAdapter.getCursor(); // get current Cursor

        if (cursor != null)
            cursor.deactivate(); // deactivate it

        positionsAdapter.changeCursor(null); // adapted now has no Cursor
        databaseConnector.close();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG,"This parameter:"+this.sorting);
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }

    // performs database query outside GUI thread
    private class GetPositionsTask extends AsyncTask<String, Object, Cursor>
    {
        // perform the database access
        @Override
        protected Cursor doInBackground(String... params)
        {
            Log.d(LOG_TAG, "GetContactTask: doInBackGround()");
            databaseConnector.open();
            // get a cursor containing call contacts
            String paramet = "";
            if( params.length > 0 ){
                paramet = params[0];
                Log.d(LOG_TAG,"AsyncTaskParam: "+ params[0]);
                Log.d(LOG_TAG,"AsyncTaskParam: "+ paramet);
            }
            else
                Log.d(LOG_TAG,"Null params");
            return databaseConnector.getAllPositions(paramet);
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            Log.d(LOG_TAG, "GetContactTask: onPostExecute()");
            positionsAdapter.changeCursor(result); // set the adapter's Cursor
            positionsAdapter.swapCursor(result);
            Log.d(LOG_TAG, "Adapter have rows: " + Integer.toString(positionsAdapter.getCount()));
            //     databaseConnector.close();
        } // end method onPostExecute
    } // end class GetContactsTask


    // event listener that responds to the user touching a contact's name
    // in the ListView
    private final AdapterView.OnItemClickListener viewPositionsListener = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
        {
            new GetMainInfoPosition().execute(positionsAdapter.getIdByPosition(arg2));
        } // end method onItemClick
    }; // end viewContactListener

    private class GetMainInfoPosition extends AsyncTask<Long, Object, Cursor>
    {
        // perform the database access
        @Override
        protected Cursor doInBackground(Long... params)
        {
            Log.d(LOG_TAG, "GetMainInfoPosition: doInBackGround()");
            databaseConnector.open();
            // get a cursor containing call contacts
            return databaseConnector.getOnePosition(params[0]);
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            Log.d(LOG_TAG, "GetMainInfoPosition: onPostExecute()");

            super.onPostExecute(result);
            result.moveToFirst(); // move to the first item
            // get the column index for each data item
            int winenameIndex = result.getColumnIndex(Constants.WINENAME);
            int imageIndex = result.getColumnIndex(Constants.IMAGE);
            int vintageIndex = result.getColumnIndex(Constants.VINTAGE);
            int producerIndex = result.getColumnIndex(Constants.PRODUCER);
            int typeIndex = result.getColumnIndex(Constants.TYPE);
            int mainmarkIndex = result.getColumnIndex(Constants.MAINMARK);
            int mainmark20Index = result.getColumnIndex(Constants.MAINMARK100);

            wine = new Wine(result.getString(winenameIndex),result.getString(imageIndex),result.getString(producerIndex),result.getString(typeIndex),result.getString(vintageIndex),null);
            sommelierMarks = new SommelierMarks(null,null,null,result.getString(mainmarkIndex),null,null,null,null,null,result.getString(mainmark20Index),null,null,null,null);
            wineMainTextView.setText(wine.getWinename());
            wineID = result.getInt(result.getColumnIndex(Constants.ID));
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;
            Bitmap bitmap = BitmapFactory.decodeFile(wine.getImage(),
                    options);
            imageMainView.setImageBitmap(bitmap);
            vintageMainTextView.setText(wine.getVintage());
            typeMainTextView.setText(wine.getType());
            producerMainTextView.setText(wine.getProducer());
            mainMarkMainTextView.setText(sommelierMarks.getMainmark());
            main20MarkTextView.setText(sommelierMarks.getMainmark100());
            //     databaseConnector.close();
        } // end method onPostExecute
    }
}