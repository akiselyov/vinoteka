package com.example.android.mypagerslidetabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Android on 07.05.2015.
 */
public class SommelierFragment extends Fragment {
    private static final String ARG_PERSON = "ARG_PERSON";
    private static final String ARG_MAINMARK = "ARG_MAINMARK";
    private static final String ARG_TASTEMARK = "ARG_TASTEMARK";
    private static final String ARG_AROMAMARK = "ARG_AROMAMARK";
    private static final String ARG_COLORMARK = "ARG_COLORMARK";
    private static final String ARG_TERROIR = "ARG_TERROIR";
    private static final String ARG_SPONTANEOUSMARK = "ARG_SPONTANEOUSMARK";

    public static SommelierFragment newInstance(SommelierMarks sommelierMarks) {
        Bundle args = new Bundle();
        Log.d("Sommelier",sommelierMarks.getAromamark() + " " + sommelierMarks.getColormark() + " " + sommelierMarks.getMainmark() + " " + sommelierMarks.getTastemark());
        args.putString(ARG_PERSON, sommelierMarks.getPerson());
        args.putString(ARG_MAINMARK, sommelierMarks.getMainmark());
        args.putString(ARG_TASTEMARK, sommelierMarks.getTastemark());
        args.putString(ARG_AROMAMARK, sommelierMarks.getAromamark());
        args.putString(ARG_COLORMARK, sommelierMarks.getColormark());
        args.putString(ARG_TERROIR, sommelierMarks.getTerroirmark());
        args.putString(ARG_SPONTANEOUSMARK, sommelierMarks.getSpontaneousmark());
        SommelierFragment fragment = new SommelierFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sommelier_layout, container, false);
        TextView personTextView = (TextView) view.findViewById(R.id.personTextView);
        TextView mainmarkTextView = (TextView) view.findViewById(R.id.mainmarkTextView);
        TextView colormarkTextView = (TextView) view.findViewById(R.id.colormarkTextView);
        TextView aromamarkTextView = (TextView) view.findViewById(R.id.aromamarkTextView);
        TextView tastemarkTextView = (TextView) view.findViewById(R.id.tastemarkTextView);
        TextView terroirTextView = (TextView) view.findViewById(R.id.terroirTextView);
        TextView spontaneousmarkTextView = (TextView) view.findViewById(R.id.spontaneousmarkTextView);
        personTextView.setText(getArguments().getString(ARG_PERSON));
        if(getArguments().getString(ARG_MAINMARK) != null)
            mainmarkTextView.setText(getArguments().getString(ARG_MAINMARK));
        if(getArguments().getString(ARG_COLORMARK) != null)
            colormarkTextView.setText(getArguments().getString(ARG_COLORMARK).substring(4));
        if(getArguments().getString(ARG_AROMAMARK) != null)
            aromamarkTextView.setText(getArguments().getString(ARG_AROMAMARK).substring(4));
        if(getArguments().getString(ARG_TASTEMARK) != null)
            tastemarkTextView.setText(getArguments().getString(ARG_TASTEMARK).substring(4));
        if(getArguments().getString(ARG_TERROIR) != null)
            terroirTextView.setText(getArguments().getString(ARG_TERROIR).substring(4));
        spontaneousmarkTextView.setText(getArguments().getString(ARG_SPONTANEOUSMARK));
        return view;
    }

}