package com.example.android.mypagerslidetabs;

/**
 * Created by Android on 16.06.2015.
 */
public class Wine {

    private String winename;
    private String image;
    private String producer;
    private String type;
    private String internet_link;
    private String vintage;

    public Wine(String winename, String image, String producer, String type, String vintage, String link) {
        this.winename = winename;
        this.image = image;
        this.producer = producer;
        this.type = type;
        this.vintage = vintage;
        this.internet_link = link;
    }

    public String getVintage() {
        return vintage;
    }

    public String getImage() {
        return image;
    }

    public String getWinename() {
        return winename;
    }

    public String getProducer() {
        return producer;
    }

    public String getType() {
        return type;
    }

    public String getInternet_link() {
        return internet_link;
    }
}
