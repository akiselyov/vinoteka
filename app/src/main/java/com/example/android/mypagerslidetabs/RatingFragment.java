package com.example.android.mypagerslidetabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Android on 23.06.2015.
 */
public class RatingFragment extends Fragment {
    private static final String ARG_MAINMARK100 = "ARG_MAINMARK";
    private static final String ARG_TASTEMARK100 = "ARG_TASTEMARK";
    private static final String ARG_AROMAMARK100 = "ARG_AROMAMARK";
    private static final String ARG_COLORMARK100 = "ARG_COLORMARK";
    private static final String ARG_TYPICALITY100 = "ARG_TYPICALITY";

    public static RatingFragment newInstance(SommelierMarks sommelierMarks) {
        Bundle args = new Bundle();
        args.putString(ARG_MAINMARK100, sommelierMarks.getMainmark100());
        args.putString(ARG_TASTEMARK100, sommelierMarks.getTastemark100());
        args.putString(ARG_AROMAMARK100, sommelierMarks.getAromamark100());
        args.putString(ARG_COLORMARK100, sommelierMarks.getColormark100());
        args.putString(ARG_TYPICALITY100, sommelierMarks.getTypicality100());
        RatingFragment fragment = new RatingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rating_layout, container, false);
        TextView mainmarkTextView = (TextView) view.findViewById(R.id.mainmark100TextView);
        TextView colormarkTextView = (TextView) view.findViewById(R.id.colormark100TextView);
        TextView aromamarkTextView = (TextView) view.findViewById(R.id.aromamark100TextView);
        TextView tastemarkTextView = (TextView) view.findViewById(R.id.tastemark100TextView);
        TextView typicalityTextView = (TextView) view.findViewById(R.id.typicalityTextView);
        if(getArguments().getString(ARG_MAINMARK100) != null)
            mainmarkTextView.setText(getArguments().getString(ARG_MAINMARK100));
        if(getArguments().getString(ARG_COLORMARK100) != null)
            colormarkTextView.setText(getArguments().getString(ARG_COLORMARK100).substring(4));
        if(getArguments().getString(ARG_AROMAMARK100) != null)
            aromamarkTextView.setText(getArguments().getString(ARG_AROMAMARK100).substring(4));
        if(getArguments().getString(ARG_TASTEMARK100) != null)
            tastemarkTextView.setText(getArguments().getString(ARG_TASTEMARK100).substring(4));
        if(getArguments().getString(ARG_TYPICALITY100) != null)
            typicalityTextView.setText(getArguments().getString(ARG_TYPICALITY100).substring(4));
        return view;
    }

}