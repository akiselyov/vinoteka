package com.example.android.mypagerslidetabs;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import database.DatabaseConnector;

/**
 * Created by Android on 09.06.2015.
 */
public class DiagramsFragment extends Fragment {

    private static final String ARG_TYPE = "TYPE";


    String[] data = {"graphic","producer","type","vintage"};


    final String LOG_TAG = "myLogs";

    public static DiagramsFragment newInstance(int position) {
        DiagramsFragment f = new DiagramsFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_TYPE, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseConnector = new DatabaseConnector(this.getActivity());
        databaseConnector.open();
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    private void openDiagram(String type) {
        GetDiagramTask getDiagramTask = new GetDiagramTask();
        getDiagramTask.execute(type);
    }

    private void createDiagram(Cursor cursor, String type) {

        ArrayList<Integer> values = new ArrayList<>();
        ArrayList<String> bars = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();
        if(cursor.getCount() != 0) {
            cursor.moveToFirst();
            int typeIndex = cursor.getColumnIndex(type);
            int type_idIndex = cursor.getColumnIndex(type + "_id");
            int countIndex = cursor.getColumnIndex("count");
            int colorvalues = 1500;
            Log.d("Error dia",type+   " " + cursor.getCount());
            do {
                values.add(cursor.getInt(countIndex));
                bars.add(cursor.getString(typeIndex));
                colors.add(Color.rgb(colorvalues,colorvalues+300,colorvalues+300));
                colorvalues += 50;
            } while (cursor.moveToNext());

            CategorySeries series = new CategorySeries("Pie Chart");  // шаг 3
            DefaultRenderer dr = new DefaultRenderer();   // шаг 4

            for (int v = 0; v < values.size(); v++) {    // шаг 5
                series.add(bars.get(v), values.get(v));
                SimpleSeriesRenderer r = new SimpleSeriesRenderer();
                r.setColor(colors.get(v));
                dr.addSeriesRenderer(r);
            }
            dr.setZoomButtonsVisible(true);
            dr.setZoomEnabled(true);
            dr.setApplyBackgroundColor(true);
            dr.setBackgroundColor(Color.WHITE);
            dr.setChartTitleTextSize(20);
            mChartView = ChartFactory.getPieChartView(getActivity(), series, dr);

            chartContainer.removeAllViews();
            // Adding the pie chart to the custom layout
            chartContainer.addView(mChartView);
        }
    }

    private void createChart(Cursor cursor) {

        ArrayList<Date> dates = new ArrayList<>();
        ArrayList<Integer> marks = new ArrayList<>();
        final ArrayList<String> names = new ArrayList<>();
        int dateIndex = cursor.getColumnIndex("date");
        int markIndex = cursor.getColumnIndex("mark");
        int wineIndex = cursor.getColumnIndex("wine");
        if(cursor.getCount()!= 0 ) {
            cursor.moveToFirst();
            do {

                int day = Integer.parseInt(cursor.getString(dateIndex).substring(0, 2));
                int month = Integer.parseInt(cursor.getString(dateIndex).substring(3,5));
                int year = Integer.parseInt(cursor.getString(dateIndex).substring(8,10));
                Log.d("DAYS",day+" "+month+" "+year);
                dates.add(new Date(year,month,day));
                marks.add(Integer.parseInt(cursor.getString(markIndex).substring(0,1)));
                names.add(cursor.getString(wineIndex));
            } while (cursor.moveToNext());

            // Creating an  XYSeries for Income
            TimeSeries incomeSeries = new TimeSeries("DateTasting");
            // Adding data to Income and Expense Series
            for (int i = 0; i < dates.size(); i++) {
                incomeSeries.add(dates.get(i),marks.get(i));
            }
            // Creating a dataset to hold each series
            XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
            // Adding Visits Series to the dataset
            dataset.addSeries(incomeSeries);
            // Creating XYSeriesRenderer to customize visitsSeries
            XYSeriesRenderer datesRenderer = new XYSeriesRenderer();
            datesRenderer.setColor(Color.RED);
            datesRenderer.setPointStyle(PointStyle.CIRCLE);
            datesRenderer.setFillPoints(true);
            datesRenderer.setLineWidth(2);
            datesRenderer.setDisplayChartValues(true);

            // Creating a XYMultipleSeriesRenderer to customize the whole chart
            XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();

            multiRenderer.setChartTitle("Days and Marks");
            multiRenderer.setXTitle("Days");
            multiRenderer.setYTitle("Mark");
            multiRenderer.setZoomButtonsVisible(true);

            // Adding visitsRenderer and viewsRenderer to multipleRenderer
            // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
            // should be same
            multiRenderer.addSeriesRenderer(datesRenderer);
            // Getting a reference to LinearLayout of the MainActivity Layout

            // Creating a Time Chart
            mChartView = (GraphicalView) ChartFactory.getTimeChartView(getActivity().getBaseContext(), dataset, multiRenderer,"dd-MM-yyyy");

            multiRenderer.setClickEnabled(true);
            multiRenderer.setSelectableBuffer(10);
            multiRenderer.setMarginsColor(Color.WHITE);
            // Setting a click event listener for the graph
            mChartView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Format formatter = new SimpleDateFormat("dd-MM-yyyy");

                    SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();

                    if (seriesSelection != null) {
                        int seriesIndex = seriesSelection.getSeriesIndex();
                        String selectedSeries="Marks";
                        if(seriesIndex==0)
                            selectedSeries = "Marks";

                        // Getting the clicked Date ( x value )
                        long clickedDateSeconds = (long) seriesSelection.getXValue();
                        Date clickedDate = new Date(clickedDateSeconds);
                        String strDate = formatter.format(clickedDate);

                        // Getting the y value
                        int amount = (int) seriesSelection.getValue();

                        // Displaying Toast Message
                        Toast.makeText(
                                getActivity().getBaseContext(),
                                selectedSeries + " on "  + strDate + " : " + amount + " " + names.get(seriesIndex),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
            chartContainer.removeAllViews();
            // Adding the pie chart to the custom layout
            chartContainer.addView(mChartView);
        }
    }

    private void openChart(String type) {
        GetDiagramTask getDiagramTask = new GetDiagramTask();
        getDiagramTask.execute(type);
    }

    private GraphicalView mChartView;
    private LinearLayout chartContainer;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "Fragment1 onCreateView");

        View view = inflater.inflate(R.layout.diagram_fragment, null);

        chartContainer = (LinearLayout) view.findViewById(R.id.chart_container);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) view.findViewById(R.id.spinnerDiagram);
        spinner.setAdapter(adapter);
        // заголовок
        spinner.setPrompt("Title");
        // выделяем элемент
        spinner.setSelection(1);
        // устанавливаем обработчик нажатия


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if(position == 0)
                    openChart(data[position]);
                else
                    openDiagram(data[position]);
                // показываем позиция нажатого элемента
                Toast.makeText(getActivity().getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        return view;
    }


    private DatabaseConnector databaseConnector;

    // performs database query outside GUI thread
    private class GetDiagramTask extends AsyncTask<String, Object, Cursor>
    {
        private String param = "";
        // perform the database access
        @Override
        protected Cursor doInBackground(String... params)
        {
            Log.d(LOG_TAG, "GetDiagramTask: doInBackGround()");
            databaseConnector.open();
            // get a cursor containing call contacts
            String paramet = "";
            if( params.length > 0 ){
                paramet = params[0];
                Log.d(LOG_TAG,"AsyncTaskParam: "+ params[0]);
                Log.d(LOG_TAG,"AsyncTaskParam: "+ paramet);
            }
            else
                Log.d(LOG_TAG,"Null params");
            param = new String(params[0]);
            if(params[0].equals("graphic"))
                return databaseConnector.getGraphicData();
            else
                return databaseConnector.getDiagramData(params[0]);
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            Log.d("ERROR", "GetDiagramTask: onPostExecute() " + param);
            if(!param.equals("graphic"))
                createDiagram(result,param);
            else
                createChart(result);
        } // end method onPostExecute
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }

}
