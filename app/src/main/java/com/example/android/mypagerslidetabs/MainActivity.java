/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.mypagerslidetabs;

import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import com.astuetz.PagerSlidingTabStrip;

import database.DatabaseConnector;

public class MainActivity extends FragmentActivity {

	private final Handler handler = new Handler();

    public static final String ROW_ID = "_id"; // Intent extra key
	private PagerSlidingTabStrip tabs;

    private Drawable oldBackground = null;
	private int currentColor = 0xFF666666;
    private final static String LOG_TAG = "LOg";

    private String[] SUGGESTIONS = { };
    private int[] identifiers;
    private SimpleCursorAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


        final String[] from = new String[] {"winename"};
        final int[] to = new int[] {R.id.barname};
        mAdapter = new SimpleCursorAdapter(this,
                R.layout.barname_layout,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        try {
            getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#5E2129")));
            getActionBar().setIcon(R.drawable.bowl);
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }

		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());

		pager.setAdapter(adapter);

		final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
				.getDisplayMetrics());
		pager.setPageMargin(pageMargin);

		tabs.setViewPager(pager);
		//changeColor(currentColor);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchItem.getActionView();
        PrepareOptionsMenu(searchItem);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
		switch (item.getItemId()) {

            case R.id.quickmenu:
                intent = new Intent(MainActivity.this, AddEditWine.class);
                startActivity(intent); // start the AddEditContact Activity
                return true;
            case R.id.action_search:
                return true;

		}
		return super.onOptionsItemSelected(item);
	}

	private void changeColor(int newColor) {

		tabs.setIndicatorColor(newColor);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

			Drawable colorDrawable = new ColorDrawable(newColor);
			Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
			LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

			if (oldBackground == null) {

				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
					ld.setCallback(drawableCallback);
				} else {
                    try {
                        getActionBar().setBackgroundDrawable(ld);
                    }
                    catch (NullPointerException ex) {
                        ex.printStackTrace();
                    }
				}

			} else {

				TransitionDrawable td = new TransitionDrawable(new Drawable[] { oldBackground, ld });

				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
					td.setCallback(drawableCallback);
				} else {
                    try {
                        getActionBar().setBackgroundDrawable(td);
                    }
                    catch (NullPointerException ex) {
                        ex.printStackTrace();
                    }
				}

				td.startTransition(200);

			}

			oldBackground = ld;

			// http://stackoverflow.com/questions/11002691/actionbar-setbackgrounddrawable-nulling-background-from-thread-handler
			getActionBar().setDisplayShowTitleEnabled(false);
			getActionBar().setDisplayShowTitleEnabled(true);

		}

		currentColor = newColor;

	}

	public void onColorClicked(View v) {

		int color = Color.parseColor(v.getTag().toString());
		changeColor(color);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("currentColor", currentColor);
	}

	@Override
	protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		//currentColor = savedInstanceState.getInt("currentColor");
	//	changeColor(currentColor);
	}

	private final Drawable.Callback drawableCallback = new Drawable.Callback() {
		@Override
		public void invalidateDrawable(Drawable who) {
            try {
                getActionBar().setBackgroundDrawable(who);
            }
            catch (NullPointerException ex) {
                ex.printStackTrace();
            }
		}

		@Override
		public void scheduleDrawable(Drawable who, Runnable what, long when) {
			handler.postAtTime(what, when);
		}

		@Override
		public void unscheduleDrawable(Drawable who, Runnable what) {
			handler.removeCallbacks(what);
		}
	};

	public class MyPagerAdapter extends FragmentPagerAdapter {

		private final String[] TITLES = { "By name", "By Producer", "By Type", "By vintages", "Diagrams", "Map" };

        private final String[] SORTING = { "wines.winename, vintages.vintage", "producers.producer", "types.type", "vintages.vintage", "sommelierratinginformation.datetasting", "sommelierratinginformation.mainmark" };

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {
            switch(position) {
                case 4:
                    return DiagramsFragment.newInstance(position);
                case 5:
                    return MapFragment.newInstance();
                default:
                    return ListWinesFragment.newInstance(position, TITLES[position], SORTING[position]);
            }
		}

	}


    private SearchView mSearchView;

    public boolean PrepareOptionsMenu(MenuItem searchItem) {

        if (isAlwaysExpanded()) {
            mSearchView.setIconifiedByDefault(false);
        } else {
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                    | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }

        mSearchView.setSuggestionsAdapter(mAdapter);

        mSearchView.setIconifiedByDefault(false);
        // Getting selected (clicked) item suggestion
        mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {
                // Your code here
                // create an Intent to launch the ViewContact Activity
                Intent viewPosition = new Intent(MainActivity.this, ViewTabsWine.class);
                // pass the selected contact's row ID as an extra with the Intent
                Cursor cursor = mAdapter.getCursor();
                for(int i = 0 ; i < position; i++) {
                    cursor.moveToNext();
                }
                int k = cursor.getInt(cursor.getColumnIndex("wine_id"));
                Log.d("bars","Trap="+Integer.toString(k));
                viewPosition.putExtra(ROW_ID,new Long(k));
                startActivity(viewPosition); // start the ViewContact Activity

                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                // Your code here
                return true;
            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                populateAdapter(s);
                return false;
            }
        });
        return false;
    }

    Cursor winenameCursor;
    // You must implements your logic to get data using OrmLite
    private void populateAdapter(String query) {
        GetWinenameTask getWinenameTask = new GetWinenameTask();
        getWinenameTask.execute(query);
    }

    protected boolean isAlwaysExpanded() {
        return false;
    }



    // performs database query outside GUI thread
    private class GetWinenameTask extends AsyncTask<String, Object, Cursor>
    {
        final DatabaseConnector databaseConnector =
                new DatabaseConnector(MainActivity.this);
        // обнуляем ссылку
        private String temp = "";
        // perform the database access
        @Override
        protected Cursor doInBackground(String... params)
        {
            Log.d(LOG_TAG, "GetContactTask: doInBackGround()");
            databaseConnector.open();
            temp.concat(params[0]);
            return databaseConnector.fetchWinesByQuery(params[0]);
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            Log.d(LOG_TAG, "GetCoordinatesTask: onPostExecute()");
            winenameCursor = result;
            SUGGESTIONS = new String[winenameCursor.getCount()];
            identifiers = new int[winenameCursor.getCount()];
            int wineIndex = winenameCursor.getColumnIndex("winename");
            int idIndex = winenameCursor.getColumnIndex("_id");
            int k  = 0;
            winenameCursor.moveToFirst();
            if(winenameCursor.getCount() != 0) {
                do {
                    SUGGESTIONS[k] = winenameCursor.getString(wineIndex);
                    identifiers[k] = winenameCursor.getInt(idIndex);
                    k++;
                } while(winenameCursor.moveToNext());
                final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "winename","wine_id" });
                for (int i=0; i<SUGGESTIONS.length; i++) {
                    if (SUGGESTIONS[i].toLowerCase().startsWith(temp.toLowerCase()))
                        c.addRow(new Object[] {i, SUGGESTIONS[i],identifiers[i]});
                }
                mAdapter.changeCursor(c);
            }
            databaseConnector.close();
        } // end method onPostExecute
    }
}