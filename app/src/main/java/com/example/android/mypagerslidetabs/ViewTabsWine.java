package com.example.android.mypagerslidetabs;

/**
 * Created by Android on 07.05.2015.
 */
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import database.DatabaseConnector;

public class ViewTabsWine extends FragmentActivity {

    public static final String ROW_ID = "_id"; // Intent extra key
    private final static String LOG_TAG = "myLogs";
    private final static String ACTIVITY_NAME = "ViewTabsWine";
    private  ViewPager viewPager;
    private PagerSlidingTabStrip tabStrip;
    private long rowID; // selected contact's name
    private Cursor loadcursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onCreate()");
        setContentView(R.layout.tabs_view_main);
        try {
            getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#5E2129")));
            getActionBar().setIcon(R.drawable.bowl);
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        viewPager = (ViewPager) findViewById(R.id.viewtabspager);
        // Give the PagerSlidingTabStrip the ViewPager
        tabStrip = (PagerSlidingTabStrip)findViewById(R.id.viewtabs);

        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
                Toast.makeText(ViewTabsWine.this,
                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });

        Bundle extras = getIntent().getExtras();
        rowID = extras.getLong(ROW_ID);
        Log.d(LOG_TAG, ACTIVITY_NAME + ": ROW_ID = "+Long.toString(rowID));
        new LoadPositionTask().execute(rowID);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onRestart()");
    }

    // called when the activity is first created
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onResume()");
        Log.d(LOG_TAG, "Rowid = " + Integer.toString((int)rowID));
        // Get the ViewPager and set it's PagerAdapter so that it can display items

        // create new LoadContactTask and execute it
   //     new LoadPositionTask().execute(rowID);
    } // end method onResume

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onDestroy()");
    }

    // performs database query outside GUI thread
    private class LoadPositionTask extends AsyncTask<Long, Object, Cursor>
    {
        final DatabaseConnector databaseConnector =
                new DatabaseConnector(ViewTabsWine.this);

        // perform the database access
        @Override
        protected Cursor doInBackground(Long... params)
        {
            databaseConnector.open();

            // get a cursor containing all data on given entry
            return databaseConnector.getOnePosition(params[0]);
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            super.onPostExecute(result);

            loadcursor = result;
            viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(), loadcursor,ViewTabsWine.this));

            // Attach the view pager to the tab strip
            tabStrip.setViewPager(viewPager);
            // Attach the page change listener to tab strip and **not** the view pager inside the activity

            databaseConnector.close(); // close database connection
        } // end method onPostExecute
    } // end class LoadContactTask


    // create the Activity's menu from a menu resource XML file
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_contact_menu, menu);
        return true;
    } // end method onCreateOptionsMenu

    // handle choice from options menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) // switch based on selected MenuItem's ID
        {
            case R.id.editItem:
                // create an Intent to launch the AddEditContact Activity
                Intent addEditContact =
                        new Intent(this, AddEditWine.class);

                new LoadPositionTask().execute(rowID);
                loadcursor.moveToFirst(); // move to the first item
                // get the column index for each data item
                int winenameIndex = loadcursor.getColumnIndex(getResources().getString(R.string.winename));
                int imageIndex = loadcursor.getColumnIndex(getResources().getString(R.string.image));
                int vintageIndex = loadcursor.getColumnIndex(getResources().getString(R.string.vintage));
                int producerIndex = loadcursor.getColumnIndex(getResources().getString(R.string.producer));
                int typeIndex = loadcursor.getColumnIndex(getResources().getString(R.string.type));
                int internetlinkIndex = loadcursor.getColumnIndex(getResources().getString(R.string.internet_link));
                int datetastingIndex = loadcursor.getColumnIndex(getResources().getString(R.string.datetasting));
                int notesIndex = loadcursor.getColumnIndex(getResources().getString(R.string.notes));
                int personIndex = loadcursor.getColumnIndex(getResources().getString(R.string.person));
                int mainmarkIndex = loadcursor.getColumnIndex(getResources().getString(R.string.mainmark));
                int colormarkIndex = loadcursor.getColumnIndex(getResources().getString(R.string.colormark));
                int aromamarkIndex = loadcursor.getColumnIndex(getResources().getString(R.string.aromamark));
                int tastemarkIndex = loadcursor.getColumnIndex(getResources().getString(R.string.tastemark));
                int terroirIndex = loadcursor.getColumnIndex(getResources().getString(R.string.terroir));
                int spontaneousmarkIndex = loadcursor.getColumnIndex(getResources().getString(R.string.spontaneousmark));
                int mainmark100 = loadcursor.getColumnIndex(Constants.MAINMARK100);
                int colormark100 = loadcursor.getColumnIndex(Constants.COLORMARK100);
                int aromamark100 = loadcursor.getColumnIndex(Constants.AROMAMARK100);
                int tastemark100 = loadcursor.getColumnIndex(Constants.TASTEMARK100);
                int typicality100 = loadcursor.getColumnIndex(Constants.TYPICALITY);
                String imagepath = loadcursor.getString(imageIndex);
                // pass the selected contact's data as extras with the Intent
                addEditContact.putExtra(MainActivity.ROW_ID, rowID);
                addEditContact.putExtra(getResources().getString(R.string.winename), loadcursor.getString(winenameIndex));
                addEditContact.putExtra(getResources().getString(R.string.image), imagepath);
                addEditContact.putExtra(getResources().getString(R.string.vintage), loadcursor.getString(vintageIndex));
                addEditContact.putExtra(getResources().getString(R.string.producer), loadcursor.getString(producerIndex));
                addEditContact.putExtra(getResources().getString(R.string.type), loadcursor.getString(typeIndex));
                addEditContact.putExtra(getResources().getString(R.string.internet_link), loadcursor.getString(internetlinkIndex));
                addEditContact.putExtra(getResources().getString(R.string.datetasting), loadcursor.getString(datetastingIndex));
                addEditContact.putExtra(getResources().getString(R.string.notes), loadcursor.getString(notesIndex));
                addEditContact.putExtra(getResources().getString(R.string.person), loadcursor.getString(personIndex));
                addEditContact.putExtra(getResources().getString(R.string.mainmark), loadcursor.getString(mainmarkIndex));
                addEditContact.putExtra(getResources().getString(R.string.colormark), loadcursor.getString(colormarkIndex));
                addEditContact.putExtra(getResources().getString(R.string.aromamark), loadcursor.getString(aromamarkIndex));
                addEditContact.putExtra(getResources().getString(R.string.tastemark), loadcursor.getString(tastemarkIndex));
                addEditContact.putExtra(getResources().getString(R.string.terroir), loadcursor.getString(terroirIndex));
                addEditContact.putExtra(getResources().getString(R.string.spontaneousmark), loadcursor.getString(spontaneousmarkIndex));
                addEditContact.putExtra(Constants.MAINMARK100,loadcursor.getString(mainmark100));
                addEditContact.putExtra(Constants.COLORMARK100,loadcursor.getString(colormark100));
                addEditContact.putExtra(Constants.TASTEMARK100,loadcursor.getString(tastemark100));
                addEditContact.putExtra(Constants.AROMAMARK100,loadcursor.getString(aromamark100));
                addEditContact.putExtra(Constants.TYPICALITY,loadcursor.getString(typicality100));
                startActivity(addEditContact); // start the Activity
                return true;
            case R.id.deleteItem:
                deletePosition(); // delete the displayed contact
                return true;
            default:
                return super.onOptionsItemSelected(item);
        } // end switch
    } // end method onOptionsItemSelected

    // delete a contact
    private void deletePosition()
    {
        // create a new AlertDialog Builder
        AlertDialog.Builder builder =
                new AlertDialog.Builder(ViewTabsWine.this);

        builder.setTitle(R.string.confirmTitle); // title bar string
        builder.setMessage(R.string.confirmMessage); // message to display

        // provide an OK button that simply dismisses the dialog
        builder.setPositiveButton(R.string.button_delete,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int button)
                    {
                        final DatabaseConnector databaseConnector =
                                new DatabaseConnector(ViewTabsWine.this);

                        // create an AsyncTask that deletes the contact in another
                        // thread, then calls finish after the deletion
                        AsyncTask<Long, Object, Object> deleteTask =
                                new AsyncTask<Long, Object, Object>()
                                {
                                    @Override
                                    protected Object doInBackground(Long... params)
                                    {
                                        databaseConnector.deletePosition(params[0]);
                                        return null;
                                    } // end method doInBackground

                                    @Override
                                    protected void onPostExecute(Object result)
                                    {
                                        finish(); // return to the AddressBook Activity
                                    } // end method onPostExecute
                                }; // end new AsyncTask

                        // execute the AsyncTask to delete contact at rowID
                        deleteTask.execute(rowID);
                    } // end method onClick
                } // end anonymous inner class
        ); // end call to method setPositiveButton

        builder.setNegativeButton(R.string.button_cancel, null);
        builder.show(); // display the Dialog
    } // end method deleteContact

}