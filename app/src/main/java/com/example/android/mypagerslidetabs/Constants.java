package com.example.android.mypagerslidetabs;

/**
 * Created by Android on 16.06.2015.
 */
public class Constants {
    public final static String ID = "_id";
    public final static String WINENAME = "winename";
    public final static String IMAGE = "image";
    public final static String PRODUCER = "producer";
    public final static String VINTAGE = "vintage";
    public final static String TYPE = "type";
    public final static String LATITUDE = "latitude";
    public final static String LONGITUDE = "longitude";
    public final static String INTERNET_LINK = "internet_link";
    public final static String DATETASTING = "datetasting";
    public final static String NOTES = "notes";
    public final static String PERSON = "person";
    public final static String MAINMARK = "mainmark";
    public final static String COLORMARK = "colormark";
    public final static String TASTEMARK = "tastemark";
    public final static String AROMAMARK = "aromamark";
    public final static String TERROIR = "terroir";
    public final static String SPONTANEOUS_MARK = "spontaneousmark";
    public final static String MAINMARK100 = "mainmark100";
    public final static String COLORMARK100 = "colormark100";
    public final static String TASTEMARK100 = "tastemark100";
    public final static String AROMAMARK100 = "aromamark100";
    public final static String TYPICALITY = "typicality100";
}
