package com.example.android.mypagerslidetabs;

/**
 * Created by Android on 25.03.2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import database.DatabaseConnector;

@SuppressWarnings("ALL")
public class AddEditWine extends Activity
{
    private final static String LOG_TAG = "AddEditWine";
    private final static String ACTIVITY_NAME = "AddEditWine";
    private GoogleMap googleMap;
    private double latitude = -1;
    private double longitude = -1;
    private boolean markerExist;
    final int TYPE_PHOTO = 1;
    private NotesFragment dialog_note;
    private final int REQUEST_CODE_PHOTO = 1;
    private Uri fileUri;
    private long rowID; // id of position being edited, if any
    // EditTexts for contact information
    private EditText winenameEditText;
    private ImageView ivPhoto;
    private AutoCompleteTextView vintageEditText;
    private AutoCompleteTextView producerEditText;
    private AutoCompleteTextView typeEditText;
    private EditText internetlinkEditText;
    private EditText datetastingEditText;
    private AutoCompleteTextView personEditText;
    private Spinner mainmarkEditText;
    private Spinner colormarkEditText;
    private Spinner tastemarkEditText;
    private Spinner aromamarkEditText;
    private Spinner terroirEditText;

    private Spinner mainmark100;
    private Spinner colormark100;
    private Spinner tastemark100;
    private Spinner aromamark100;
    private Spinner typicality100;

    private EditText spontaneousmarkEditText;
    private String imagepath;
    private String notepath;
    private ArrayAdapter produceradapter;
    private ArrayAdapter typeadapter;
    private ArrayAdapter vintageadapter;
    private ArrayAdapter personadapter;

    private TextView mainmarkLabel;
    private TextView colormarkLabel;
    private TextView tastemarkLabel;
    private TextView aromamarkLabel;
    private TextView terroirmarkLabel;

    private Wine wine;
    private Coordinates coordinates;
    private SommelierMarks sommelierMarks;

    String[] mainmarks = {"1 - Very Bad", "2 - Bad", "3 - Normal", "4 - Good", "5 - Nice"};
    String[] aromamarks = {"0 - Разлаженный", "2 - Грубый","3 - Маловинный","4 - Чистый","5 - Винный"};
    String[] colormarks = {"0 - Грязный","2 - Не соответствует сорту","3 - Винно-желтый","4 - Хороший цвет","5 - Рубиновый"};
    String[] tastemark = {"1 - Грубый","2 - Маловинный", "3 - Слабовинный","4 - Чистый","5 - Полный"};
    String[] claritymark = {"1 - Мутное","2 - Мутноватое","3 - Чистое","4 - Прозрачное","5 - С блеском"};

    String[] mainmarks100 = {"1 - Very Bad", "2 - Bad", "3 - Normal", "4 - Good", "5 - Nice"};
    String[] aromamarks100 = {"4 - Очень хорошо", "3 - Хорошо","2 - Приемлемо","1 - Посредственно","0 - Неприемлемо"};
    String[] colormarks100 = {"4 - Очень хорошо", "3 - Хорошо","2 - Приемлемо","1 - Посредственно","0 - Неприемлемо"};
    String[] tastemarks100 = {"4 - Очень хорошо", "3 - Хорошо","2 - Приемлемо","1 - Посредственно","0 - Неприемлемо"};
    String[] typicalitymark100 = {"4 - Очень хорошо", "3 - Хорошо","2 - Приемлемо","1 - Посредственно","0 - Неприемлемо"};
    private int selecteditem = 1;
    /**
     * Initialises the mapview
     */
    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == googleMap) {
                googleMap = ((com.google.android.gms.maps.MapFragment) getFragmentManager().findFragmentById(
                        R.id.mapViewAdd)).getMap();

                if (null == googleMap) {
                    Toast.makeText(getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("mapApp", exception.toString());
        }
    }

    // called when the Activity is first started
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState); // call super's onCreate
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onCreate()");
        setContentView(R.layout.add_position); // inflate the UI
        fileUri = null;
        dialog_note = new NotesFragment();
        // get the defined string-array
        //ArrayAdapter<String> mainadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mainmarks);
        //mainadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> aromaadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, aromamarks);
        aromaadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> coloradapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, colormarks);
        coloradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> tasteadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tastemark);
        tasteadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> clarityadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, claritymark);
        clarityadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //ArrayAdapter<String> mainmark100adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mainmarks100);
        //mainadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> colormark100adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, colormarks100);
        colormark100adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> tastemark100adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tastemarks100);
        tastemark100adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> aromamarks100adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, aromamarks100);
        aromamarks100adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> typicality100adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, typicalitymark100);
        typicality100adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        try {
            getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#5E2129")));
            getActionBar().setIcon(R.drawable.bowl);
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        winenameEditText = (EditText) findViewById(R.id.winenameEditText);
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
        vintageEditText = (AutoCompleteTextView) findViewById(R.id.vintageEditText);
        new GetListTask().execute("vintage");
        producerEditText = (AutoCompleteTextView) findViewById(R.id.producerEditText);
        new GetListTask().execute("producer");
        typeEditText = (AutoCompleteTextView) findViewById(R.id.typeEditText);
        new GetListTask().execute("type");
        internetlinkEditText = (EditText) findViewById(R.id.internetlinkEditText);
        datetastingEditText = (EditText) findViewById(R.id.datetastingEditText);
        personEditText = (AutoCompleteTextView) findViewById(R.id.personEditText);
        new GetListTask().execute("person");

        //mainmarkLabel = (TextView) findViewById(R.id.mainmarkEditText);
        colormarkLabel = (TextView) findViewById(R.id.colormarkEditText);
        aromamarkLabel = (TextView) findViewById(R.id.aromamarkEditText);
        tastemarkLabel = (TextView) findViewById(R.id.tastemarkEditText);
        terroirmarkLabel = (TextView) findViewById(R.id.terroirEditText);

      /*  mainmarkEditText = (Spinner) findViewById(R.id.mainmark_spinner);
        mainmarkEditText.setAdapter(mainadapter);
        mainmarkEditText.setPrompt("Title");
        mainmarkEditText.setSelection(selecteditem);*/
        colormarkEditText = (Spinner) findViewById(R.id.colormark_spinner);
        colormarkEditText.setAdapter(coloradapter);
        colormarkEditText.setPrompt("Title");
        colormarkEditText.setSelection(selecteditem);
        aromamarkEditText = (Spinner) findViewById(R.id.aromamark_spinner);
        aromamarkEditText.setAdapter(aromaadapter);
        aromamarkEditText.setPrompt("Title");
        aromamarkEditText.setSelection(selecteditem);
        tastemarkEditText = (Spinner) findViewById(R.id.tastemark_spinner);
        tastemarkEditText.setAdapter(tasteadapter);
        tastemarkEditText.setPrompt("Title");
        tastemarkEditText.setSelection(selecteditem);
        terroirEditText = (Spinner) findViewById(R.id.terroir_spinner);
        terroirEditText.setAdapter(clarityadapter);
        terroirEditText.setPrompt("Title");
        terroirEditText.setSelection(selecteditem);
        spontaneousmarkEditText = (EditText) findViewById(R.id.spontaneousmarkEditText);

     /*   mainmark100 = (Spinner) findViewById(R.id.mainmark100_spinner);
        mainmark100.setAdapter(mainmark100adapter);
        mainmark100.setPrompt("Title");
        mainmark100.setSelection(selecteditem);*/

        colormark100 = (Spinner) findViewById(R.id.colormark100_spinner);
        colormark100.setAdapter(colormark100adapter);
        colormark100.setPrompt("Title");
        colormark100.setSelection(selecteditem);

        tastemark100 = (Spinner) findViewById(R.id.tastemark100_spinner);
        tastemark100.setAdapter(tastemark100adapter);
        tastemark100.setPrompt("Title");
        tastemark100.setSelection(selecteditem);

        aromamark100 = (Spinner) findViewById(R.id.aromamark100_spinner);
        aromamark100.setAdapter(aromamarks100adapter);
        aromamark100.setPrompt("Title");
        aromamark100.setSelection(selecteditem);

        typicality100 = (Spinner) findViewById(R.id.typicality100_spinner);
        typicality100.setAdapter(typicality100adapter);
        typicality100.setPrompt("Title");
        typicality100.setSelection(selecteditem);
        Bundle extras = getIntent().getExtras(); // get Bundle of extras

        // для tvColor и tvSize необходимо создавать контекстное меню
      //  registerForContextMenu(mainmarkEditText);
        registerForContextMenu(colormarkEditText);
        registerForContextMenu(aromamarkEditText);
        registerForContextMenu(tastemarkEditText);
        registerForContextMenu(terroirEditText);
       // registerForContextMenu(mainmark100);
        registerForContextMenu(colormark100);
        registerForContextMenu(tastemark100);
        registerForContextMenu(aromamark100);
        registerForContextMenu(typicality100);

        createMapView();
        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                if(!markerExist) {
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.draggable(true);
                    //create new marker when user long clicks
                    googleMap.addMarker(markerOptions);
                    LatLng dragPosition = markerOptions.getPosition();
                    double dragLat = dragPosition.latitude;
                    double dragLong = dragPosition.longitude;
                    dragLat = dragLat * 1000;
                    int i = (int) Math.round(dragLat);
                    dragLat = (double)i / 1000;
                    dragLong = dragLong * 1000;
                    i = (int) Math.round(dragLong);
                    dragLong = (double)i/1000;
                    latitude = dragLat;
                    longitude = dragLong;
                    Log.i("GOOGLEMAP", "on drag end :" + dragLat + " dragLong :" + dragLong);
                    Toast.makeText(getApplicationContext(), "on drag end :" + dragLat + " dragLong :" + dragLong, Toast.LENGTH_LONG).show();
                    markerExist = true;
                }

            }
        });

        markerExist = false;
        // if there are extras, use them to populate the EditTexts
        if (extras != null)
        {
            rowID = extras.getLong(MainActivity.ROW_ID);
            winenameEditText.setText(extras.getString(Constants.WINENAME));
            imagepath = extras.getString(getResources().getString(R.string.image));
            Log.d("PHOTO","Beggining = "+imagepath);
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;
            Bitmap bitmap = BitmapFactory.decodeFile(imagepath,
                    options);

            ivPhoto.setImageBitmap(bitmap);
            vintageEditText.setText(extras.getString(Constants.VINTAGE));
            producerEditText.setText(extras.getString(Constants.PRODUCER));

            double dragLat = extras.getDouble(Constants.LATITUDE);
            double dragLong = extras.getDouble(Constants.LONGITUDE);
            LatLng dragPosition = new LatLng(dragLat,dragLong);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(dragPosition);
            markerOptions.draggable(true);
            //create new marker when user long clicks
            googleMap.addMarker(markerOptions);
            typeEditText.setText(extras.getString(Constants.TYPE));
            internetlinkEditText.setText(extras.getString(Constants.INTERNET_LINK));
            datetastingEditText.setText(extras.getString(Constants.DATETASTING));
            notepath = extras.getString(Constants.NOTES);
            personEditText.setText(extras.getString(Constants.PERSON));
            spontaneousmarkEditText.setText(extras.getString(Constants.SPONTANEOUS_MARK));
        } // end if
        else {
            FileManager fileManager = new FileManager(AddEditWine.this);
            notepath = fileManager.generateFile(2);
        }
        Log.d("PHOTO","Beggining = "+imagepath);
        // set event listener for the Save Position Button
        Button savePositionButton =
                (Button) findViewById(R.id.savePositionButton);
        savePositionButton.setOnClickListener(savePositionButtonClicked);

    } // end method onCreate


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onStart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, ACTIVITY_NAME + ": onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, ACTIVITY_NAME + "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, ACTIVITY_NAME + "onDestroy()");
    }

    // responds to event generated when user clicks the Done Button
    private final OnClickListener savePositionButtonClicked = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (winenameEditText.getText().length() != 0)
            {
                AsyncTask<Object, Object, Object> saveContactTask =
                        new AsyncTask<Object, Object, Object>()
                        {
                            @Override
                            protected Object doInBackground(Object... params)
                            {
                                Log.d(LOG_TAG, "savePosition");
                                savePosition(); // save contact to the database
                                return null;
                            } // end method doInBackground

                            @Override
                            protected void onPostExecute(Object result)
                            {
                                finish(); // return to the previous Activity
                            } // end method onPostExecute
                        }; // end AsyncTask

                // save the contact to the database using a separate thread
                saveContactTask.execute((Object[]) null);
            } // end if
            else
            {
                // create a new AlertDialog Builder
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(AddEditWine.this);

                // set dialog title & message, and provide Button to dismiss
                builder.setTitle(R.string.errorTitle);
                builder.setMessage(R.string.errorMessage);
                builder.setPositiveButton(R.string.errorButton, null);
                builder.show(); // display the Dialog
            } // end else
        } // end method onClick
    }; // end OnClickListener saveContactButtonClicked

    // saves contact information to the database
    private void savePosition()
    {
        // get DatabaseConnector to interact with the SQLite database
        DatabaseConnector databaseConnector = new DatabaseConnector(this);

        if (getIntent().getExtras() == null)
        {
           // String notespath = dialog_note.getFileName();
            // insert the contact information into the database

            double d = this.countMark(colormarkEditText.getSelectedItem().toString(),aromamarkEditText.getSelectedItem().toString(),tastemarkEditText.getSelectedItem().toString(),terroirEditText.getSelectedItem().toString(),typicality100.getSelectedItem().toString(),"20");
            d = d * 10;
            int i = (int) Math.round(d);
            d = (double)i / 10;

            double d2 = this.countMark(colormark100.getSelectedItem().toString(),aromamark100.getSelectedItem().toString(),tastemark100.getSelectedItem().toString(),terroirEditText.getSelectedItem().toString(),typicality100.getSelectedItem().toString(),"100");
            d2 = d2 * 10;
            int i2 = (int) Math.round(d2);
            d2 = (double)i2 / 10;

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            String strDate = sdf.format(c.getTime());

            wine = new Wine(
                    winenameEditText.getText().toString(),
                    imagepath,
                    producerEditText.getText().toString(),
                    typeEditText.getText().toString(),
                    vintageEditText.getText().toString(),
                    internetlinkEditText.getText().toString());
            coordinates = new Coordinates(
                    latitude,
                    longitude);
            sommelierMarks = new SommelierMarks(
                    strDate,
                    notepath,
                    personEditText.getText().toString(),
                    Double.toString(d),
                    colormarkEditText.getSelectedItem().toString(),
                    aromamarkEditText.getSelectedItem().toString(),
                    tastemarkEditText.getSelectedItem().toString(),
                    terroirEditText.getSelectedItem().toString(),
                    spontaneousmarkEditText.getText().toString(),
                    Double.toString(d2),
                    colormark100.getSelectedItem().toString(),
                    tastemark100.getSelectedItem().toString(),
                    typicality100.getSelectedItem().toString(),
                    aromamark100.getSelectedItem().toString());

            databaseConnector.insertNewPosition(
                    wine.getWinename(),
                    wine.getImage(),
                    wine.getVintage(),
                    wine.getProducer(),
                    wine.getType(),
                    wine.getInternet_link(),
                    sommelierMarks.getPerson(),
                    sommelierMarks.getDatetasting(),
                    sommelierMarks.getNotes(),
                    sommelierMarks.getMainmark(),
                    sommelierMarks.getColormark(),
                    sommelierMarks.getAromamark(),
                    sommelierMarks.getTastemark(),
                    sommelierMarks.getTerroirmark(),
                    sommelierMarks.getSpontaneousmark(),
                    coordinates.getLongitude(),
                    coordinates.getLatitude(),
                    sommelierMarks.getMainmark100(),
                    sommelierMarks.getColormark100(),
                    sommelierMarks.getAromamark100(),
                    sommelierMarks.getTastemark100(),
                    sommelierMarks.getTypicality100()
            );


        } // end if
        else
        {

            double d = this.countMark(colormarkEditText.getSelectedItem().toString(),aromamarkEditText.getSelectedItem().toString(),tastemarkEditText.getSelectedItem().toString(),terroirEditText.getSelectedItem().toString(),typicality100.getSelectedItem().toString(),"20");
            d = d * 10;
            int i = (int) Math.round(d);
            d = (double)i / 10;

            double d2 = this.countMark(colormark100.getSelectedItem().toString(),aromamark100.getSelectedItem().toString(),tastemark100.getSelectedItem().toString(),terroirEditText.getSelectedItem().toString(),typicality100.getSelectedItem().toString(),"100");
            d2 = d2 * 10;
            int i2 = (int) Math.round(d2);
            d2 = (double)i2 / 10;


            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            String strDate = sdf.format(c.getTime());

            wine = new Wine(
                    winenameEditText.getText().toString(),
                    imagepath,
                    producerEditText.getText().toString(),
                    typeEditText.getText().toString(),
                    vintageEditText.getText().toString(),
                    internetlinkEditText.getText().toString());
            coordinates = new Coordinates(
                    latitude,
                    longitude);
            sommelierMarks = new SommelierMarks(
                    strDate,
                    notepath,
                    personEditText.getText().toString(),
                    Double.toString(d),
                    colormarkEditText.getSelectedItem().toString(),
                    aromamarkEditText.getSelectedItem().toString(),
                    tastemarkEditText.getSelectedItem().toString(),
                    terroirEditText.getSelectedItem().toString(),
                    spontaneousmarkEditText.getText().toString(),
                    Double.toString(d2),
                    colormark100.getSelectedItem().toString(),
                    tastemark100.getSelectedItem().toString(),
                    typicality100.getSelectedItem().toString(),
                    aromamark100.getSelectedItem().toString());

            databaseConnector.updatePosition(rowID,
                    wine.getWinename(),
                    wine.getImage(),
                    wine.getInternet_link(),
                    sommelierMarks.getDatetasting(),
                    sommelierMarks.getNotes(),
                    sommelierMarks.getMainmark(),
                    sommelierMarks.getColormark(),
                    sommelierMarks.getAromamark(),
                    sommelierMarks.getTastemark(),
                    sommelierMarks.getTerroirmark(),
                    sommelierMarks.getSpontaneousmark(),
                    wine.getProducer(),
                    wine.getType(),
                    wine.getVintage(),
                    sommelierMarks.getMainmark100(),
                    sommelierMarks.getColormark100(),
                    sommelierMarks.getAromamark100(),
                    sommelierMarks.getTastemark100(),
                    sommelierMarks.getTypicality100()
            );
        } // end else
    } // end class saveContact

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == REQUEST_CODE_PHOTO) {
            // bitmap factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);
            Log.d("PHOTO","photoPath = "+fileUri.getPath());
            imagepath = fileUri.getPath();
            ivPhoto.setImageBitmap(bitmap);

            if (resultCode == RESULT_OK) {
                if (intent == null) {
                    Log.d(LOG_TAG, "Intent is null");
                } else {
                    Log.d(LOG_TAG, "Photo uri: " + intent.getData());
                    Bundle bndl = intent.getExtras();
                    if (bndl != null) {
                        Object obj = intent.getExtras().get("data");
                        if (obj instanceof Bitmap) {
                            bitmap = (Bitmap) obj;
                            Log.d(LOG_TAG, "bitmap " + bitmap.getWidth() + " x "
                                    + bitmap.getHeight());
                            ivPhoto.setImageBitmap(bitmap);
                        }
                    }
                    else
                        Log.d(LOG_TAG,"it is null!");
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(LOG_TAG, "Canceled");
            }
        }

    }

    public void onClickPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        FileManager fm = new FileManager(this);
        fileUri = fm.generateFileUri(TYPE_PHOTO);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);
     //   imageEditText.setText(fileUri.getPath().toString());
    }

    public void onClickNote(View view) {
        dialog_note.setFileName(notepath);
        Log.d(LOG_TAG,"Note path such: " + notepath);
        dialog_note.show(getFragmentManager(), "dialog_note");
    }


    // performs database query outside GUI thread
    private class GetListTask extends AsyncTask<String, Object, Cursor>
    {
        DatabaseConnector databaseConnector;
        String parameter = "";
        // perform the database access
        @Override
        protected Cursor doInBackground(String... params)
        {
            this.parameter = this.parameter.concat(params[0]);
            databaseConnector = new DatabaseConnector(AddEditWine.this);
            databaseConnector.open();
            return databaseConnector.getList(params[0]);
        } // end method doInBackground

        // use the Cursor returned from the doInBackground method
        @Override
        protected void onPostExecute(Cursor result)
        {
            if(parameter.equals("producer")) {
                ArrayList<String> labels = new ArrayList<String>();
                for(result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
                    labels.add(result.getString(result.getColumnIndex("producer")));
                    Log.d(LOG_TAG,"Producers: " + result.getString(result.getColumnIndex("producer")));
                }
                produceradapter = new ArrayAdapter<String>(AddEditWine.this,android.R.layout.simple_list_item_1,labels.toArray(new String[labels.size()]));
                producerEditText.setAdapter(produceradapter);
                producerEditText.setThreshold(1);
            }
            else if(parameter.equals("vintage")) {
                ArrayList<String> labels = new ArrayList<String>();
                for(result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
                    labels.add(result.getString(result.getColumnIndex("vintage")));
                    Log.d(LOG_TAG,"Vintages: " + result.getString(result.getColumnIndex("vintage")));
                }
                vintageadapter = new ArrayAdapter<String>(AddEditWine.this,android.R.layout.simple_list_item_1,labels.toArray(new String[labels.size()]));
                vintageEditText.setAdapter(vintageadapter);
                vintageEditText.setThreshold(1);
            }
            else if(parameter.equals("type")) {
                ArrayList<String> labels = new ArrayList<String>();
                for(result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
                    labels.add(result.getString(result.getColumnIndex("type")));
                    Log.d(LOG_TAG,"Types: " + result.getString(result.getColumnIndex("type")));
                }
                typeadapter = new ArrayAdapter<String>(AddEditWine.this,android.R.layout.simple_list_item_1,labels.toArray(new String[labels.size()]));
                typeadapter.notifyDataSetChanged();
                typeEditText.setAdapter(typeadapter);
                typeEditText.setThreshold(1);
            }
            else if(parameter.equals("person")) {
                ArrayList<String> labels = new ArrayList<String>();
                for(result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
                    labels.add(result.getString(result.getColumnIndex("person")));
                    Log.d(LOG_TAG,"Persons: " + result.getString(result.getColumnIndex("person")));
                }
                personadapter = new ArrayAdapter<String>(AddEditWine.this,android.R.layout.simple_list_item_1,labels.toArray(new String[labels.size()]));
                personadapter.notifyDataSetChanged();
                personEditText.setAdapter(personadapter);
                personEditText.setThreshold(1);
            }
            databaseConnector.close();
        } // end method onPostExecute
    }

    private double countMark(String color, String aroma, String taste, String clarity, String typicality, String system) {
        if(system.equals("20"))
            return Integer.parseInt(color.substring(0,1))*0.4 + Integer.parseInt(clarity.substring(0,1))*0.4 + Integer.parseInt(taste.substring(0,1))*2 + Integer.parseInt(aroma.substring(0,1))*1.2 ;
        else
            return Integer.parseInt(color.substring(0,1))*4 + Integer.parseInt(typicality.substring(0,1))*6 + Integer.parseInt(taste.substring(0,1))*9 + Integer.parseInt(aroma.substring(0,1))*6 ;
    }
}