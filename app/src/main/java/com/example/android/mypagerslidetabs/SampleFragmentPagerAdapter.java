package com.example.android.mypagerslidetabs;

import android.app.Activity;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Android on 07.05.2015.
 */
class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private final String tabTitles[] = new String[] { "Основное", "Опции", "Оценки 20", "Оценки 100"};

    private Wine wine;
    private SommelierMarks sommelierMarks;
    private Coordinates coordinates;

    public SampleFragmentPagerAdapter(FragmentManager fm, Cursor loadcursor, Activity activity) {
        super(fm);
        Activity act = activity;
        Cursor loadcurs = loadcursor;
        loadcurs.moveToFirst(); // move to the first item

        // get the column index for each data item

        int winenameIndex = loadcurs.getColumnIndex(Constants.WINENAME);
        int imageIndex = loadcurs.getColumnIndex(Constants.IMAGE);
        int vintageIndex = loadcurs.getColumnIndex(Constants.VINTAGE);
        int producerIndex = loadcurs.getColumnIndex(Constants.PRODUCER);
        int latitudeIndex = loadcursor.getColumnIndex(Constants.LATITUDE);
        int longitudeIndex = loadcursor.getColumnIndex(Constants.LONGITUDE);
        int typeIndex = loadcurs.getColumnIndex(Constants.TYPE);
        int internetlinkIndex = loadcurs.getColumnIndex(Constants.INTERNET_LINK);
        int datetastingIndex = loadcurs.getColumnIndex(Constants.DATETASTING);
        int notesIndex = loadcurs.getColumnIndex(Constants.NOTES);
        int personIndex = loadcurs.getColumnIndex(Constants.PERSON);
        int mainmarkIndex = loadcurs.getColumnIndex(Constants.MAINMARK);
        int colormarkIndex = loadcurs.getColumnIndex(Constants.COLORMARK);
        int aromamarkIndex = loadcurs.getColumnIndex(Constants.AROMAMARK);
        int tastemarkIndex = loadcurs.getColumnIndex(Constants.TASTEMARK);
        int terroirIndex = loadcurs.getColumnIndex(Constants.TERROIR);
        int spontaneousmarkIndex = loadcurs.getColumnIndex(Constants.SPONTANEOUS_MARK);
        int mainmark100Index = loadcurs.getColumnIndex(Constants.MAINMARK100);
        int colormark100Index = loadcurs.getColumnIndex(Constants.COLORMARK100);
        int aromamark100Index = loadcurs.getColumnIndex(Constants.AROMAMARK100);
        int tastemark100Index = loadcurs.getColumnIndex(Constants.TASTEMARK100);
        int typicality100Index = loadcurs.getColumnIndex(Constants.TYPICALITY);

        wine = new Wine(loadcurs.getString(winenameIndex),
                loadcurs.getString(imageIndex),
                loadcurs.getString(producerIndex),
                loadcurs.getString(typeIndex),
                loadcurs.getString(vintageIndex),
                loadcurs.getString(internetlinkIndex));
        coordinates = new Coordinates(loadcursor.getDouble(latitudeIndex),loadcursor.getDouble(longitudeIndex));
        sommelierMarks = new SommelierMarks(loadcurs.getString(datetastingIndex),
                loadcurs.getString(notesIndex),
                loadcurs.getString(personIndex),
                loadcurs.getString(mainmarkIndex),
                loadcurs.getString(colormarkIndex),
                loadcurs.getString(aromamarkIndex),
                loadcurs.getString(tastemarkIndex),
                loadcurs.getString(terroirIndex),
                loadcurs.getString(spontaneousmarkIndex),
                loadcurs.getString(mainmark100Index),
                loadcurs.getString(colormark100Index),
                loadcurs.getString(tastemark100Index),
                loadcurs.getString(typicality100Index),
                loadcurs.getString(aromamark100Index));
    }

    @Override
    public int getCount() {
        int PAGE_COUNT = 4;
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return MainInfoFragment.newInstance(this.wine, this.coordinates);
            case 1:
                return OptionsInfoFragment.newInstance(this.wine,this.sommelierMarks);
            case 2:
                return SommelierFragment.newInstance(this.sommelierMarks);
            case 3:
                return RatingFragment.newInstance(this.sommelierMarks);
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

}