package com.example.android.mypagerslidetabs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Android on 07.05.2015.
 */
public class MainInfoFragment extends Fragment {
    private static final String ARG_WINENAME = "WINENAME";
    private static final String ARG_IMAGE = "IMAGE";
    private static final String ARG_VINTAGE = "VINTAGE";
    private static final String ARG_PRODUCER = "PRODUCER";
    private static final String ARG_TYPE = "TYPE";
    private static final String ARG_LATITUDE = "LATITUDE";
    private static final String ARG_LONGITUDE = "LONGITUDE";

    private GoogleMap googleMap;


    public static MainInfoFragment newInstance(Wine wine, Coordinates coordinates) {
        Bundle args = new Bundle();
        args.putString(ARG_WINENAME, wine.getWinename());
        args.putString(ARG_IMAGE, wine.getImage());
        args.putString(ARG_VINTAGE, wine.getVintage());
        args.putString(ARG_PRODUCER, wine.getProducer());
        args.putString(ARG_TYPE, wine.getType());
        args.putDouble(ARG_LATITUDE, coordinates.getLatitude());
        args.putDouble(ARG_LONGITUDE,  coordinates.getLongitude());
        MainInfoFragment fragment = new MainInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Initialises the mapview
     */
    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == googleMap) {
                googleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(
                        R.id.mapViewMain)).getMap();
                /**
                 * If the map is still null after attempted initialisation,
                 * show an error to the user
                 */
                if (null == googleMap) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("mapApp", exception.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean markerExist = false;
    }

    private static View rootView;
    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //        <!--FrameLayout"com.google.android.gms.maps.MapFragment"-->
       // View view = inflater.inflate(R.layout.main_info_layout, container, false);

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.main_info_layout, container, false);

        } catch (InflateException e) {
        /* map is already there, just return view as it is  */
        }


        TextView winenameTextView = (TextView) rootView.findViewById(R.id.winenameTextView);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageTabView);
        TextView vintageTextView = (TextView) rootView.findViewById(R.id.vintageTextView);
        TextView producerTextView = (TextView) rootView.findViewById(R.id.producerTextView);
        TextView typeTextView = (TextView) rootView.findViewById(R.id.typeTextView);

        winenameTextView.setText(getArguments().getString(ARG_WINENAME));
        // imageTextView.setText(result.getString(imageIndex));
        if(getArguments().getString(ARG_IMAGE) != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            Bitmap bitmap = BitmapFactory.decodeFile(getArguments().getString(ARG_IMAGE),
                    options);

            imageView.setImageBitmap(bitmap);
        }
        vintageTextView.setText(getArguments().getString(ARG_VINTAGE));
        producerTextView.setText(getArguments().getString(ARG_PRODUCER));
        typeTextView.setText(getArguments().getString(ARG_TYPE));
        createMapView();

        double dragLat = getArguments().getDouble(ARG_LATITUDE);
        double dragLong = getArguments().getDouble(ARG_LONGITUDE);
        LatLng dragPosition = new LatLng(dragLat,dragLong);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(dragPosition);
        markerOptions.draggable(true);
        //create new marker when user long clicks
        googleMap.addMarker(markerOptions);
        Toast.makeText(getActivity().getApplicationContext(), "on drag end :" + dragLat + " dragLong :" + dragLong, Toast.LENGTH_LONG).show();
        return rootView;
    }

}