package com.example.android.mypagerslidetabs;

/**
 * Created by Android on 16.06.2015.
 */
public class SommelierMarks {

    private String datetasting;
    private String notes;
    private String person;
    private String mainmark;
    private String colormark;
    private String aromamark;
    private String tastemark;
    private String terroirmark;
    private String spontaneousmark;
    private String mainmark100;
    private String colormark100;
    private String tastemark100;
    private String typicality100;
    private String aromamark100;

    public SommelierMarks(String datetasting, String notes, String person, String mainmark, String colormark, String aromamark, String tastemark, String terroirmark, String spontaneousmark,
                          String mainmark100, String colormark100, String tastemark100, String typicality100, String aromamark100) {
        this.datetasting = datetasting;
        this.notes = notes;
        this.person = person;
        this.mainmark = mainmark;
        this.colormark = colormark;
        this.aromamark = aromamark;
        this.tastemark = tastemark;
        this.terroirmark = terroirmark;
        this.spontaneousmark = spontaneousmark;
        this.mainmark100 = mainmark100;
        this.colormark100 = colormark100;
        this.tastemark100 = tastemark100;
        this.typicality100 = typicality100;
        this.aromamark100 = aromamark100;
    }

    public String getDatetasting() {
        return datetasting;
    }

    public String getNotes() {
        return notes;
    }

    public String getMainmark() {
        return mainmark;
    }

    public String getColormark() {
        return colormark;
    }

    public String getAromamark() {
        return aromamark;
    }

    public String getTastemark() {
        return tastemark;
    }

    public String getTerroirmark() {
        return terroirmark;
    }

    public String getSpontaneousmark() {
        return spontaneousmark;
    }

    public String getPerson() { return person; }

    public String getMainmark100() { return mainmark100; }

    public String getColormark100() { return colormark100; }

    public String getTastemark100() { return tastemark100; }

    public String getTypicality100() { return typicality100; }

    public String getAromamark100() { return aromamark100; }
}
