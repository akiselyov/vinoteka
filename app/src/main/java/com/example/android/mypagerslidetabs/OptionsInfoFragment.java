package com.example.android.mypagerslidetabs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Android on 07.05.2015.
 */
public class OptionsInfoFragment extends Fragment {
    private static final String ARG_INTERNET_LINK = "INTERNET_LINK";
    private static final String ARG_DATETASTING = "DATETASTING";
    private static final String ARG_NOTE = "NOTE";

    private static String internet_link;
    private static String datetasting;
    private static String note;

    public static OptionsInfoFragment newInstance(Wine wine, SommelierMarks sommelierMarks) {
        Bundle args = new Bundle();
        args.putString(ARG_INTERNET_LINK, wine.getInternet_link());
        args.putString(ARG_DATETASTING, sommelierMarks.getDatetasting());
        args.putString(ARG_NOTE, sommelierMarks.getNotes());
        OptionsInfoFragment fragment = new OptionsInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internet_link = getArguments().getString(ARG_INTERNET_LINK);
        datetasting = getArguments().getString(ARG_DATETASTING);
        note = getArguments().getString(ARG_NOTE);
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.optional_info_layout, container, false);
        TextView internetlinkTextView = (TextView) view.findViewById(R.id.internetlinkTextView);
        TextView datetastingTextView = (TextView) view.findViewById(R.id.datetastingTextView);
        TextView notesTextView = (TextView) view.findViewById(R.id.notesTextView);
        internetlinkTextView.setText(internet_link);
        datetastingTextView.setText(datetasting);

        if(note != null) {
            FileManager fileManager = new FileManager(OptionsInfoFragment.this.getActivity());
            notesTextView.setText(fileManager.readFile(note));
        }
        Button linkButton = (Button) view.findViewById(R.id.btnLnk);
        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (internet_link != null)
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(internet_link)));
            }
        });
        return view;
    }
}